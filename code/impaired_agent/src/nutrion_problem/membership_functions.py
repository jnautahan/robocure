class MembershipFunctions:
    '''
    Class to keep track of all the different membership functions 
    - for each fuzzy variable, a new membership function should be created
    - membership functions should be tuned to resemble the real data
    '''
    def __init__(self, fuzVar, domain):
        self.fuzVar = fuzVar
        self.domain = domain 

    #Define the shape of all membership functions
    # - takes as arguments: domain as np.array, mean and std as number
    def membership_shape(x, mean, std):
        power = -(x-mean)/std 
        return np.exp(power*power)