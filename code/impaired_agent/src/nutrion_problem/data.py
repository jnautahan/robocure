# data = [
#     dict(name='sandwich',
#     type='ceasar', 
#     carbs=dict(mean=150, std=10)),
#     dict(name='sandwich',
#     type='salmon',
#     carbs=dict(mean=125, std=10))
# ]

# print(data[0].get('carbs').get('mean'))
import sqlite3 
con = sqlite3.connect('test.db')
with con:
    cur = con.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS lunch(name TEXT, type TEXT)")
    cur.execute("INSERT INTO lunch VALUES('sandwich', 'ham')")
    cur.execute("INSERT INTO lunch VALUES('pizza', 'margherita')")

