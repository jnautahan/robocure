'''
    Generate the database using the dataset Python package
    * Data is stored in tables, which can be indexed easily
    * for documentation on dataset see: https://dataset.readthedocs.io/en/latest/ 
    * for documentation on datafreese see: https://github.com/pudo/datafreeze 

    All of the dictionary entries are given per serving, which is denoted in weight
    - name = name of the objects at hand
    - weight = weight of one serving (in g) [note: these are currently VERY crude and sometimes pulled out of thin air]
    - calories = calories of one serving (in kcal)
    - fat = fat contents of one serving (in g) [note: currently no distinction between (un)saturated fats]
    - carbs = carbohydrates per serving (in g)
'''
# Import libraries:
# - dataset allow us to instantiate .db files and add indexable tables
# - datafreeze.app allows us to save these tables in specific formats (json, csv, ..)
# Note: Using SQLite, we create/connect a database data.db, which we can call elsewhere by instantiating
#       SQLite should be working with every version of Python
import dataset
from datafreeze.app import freeze
import numpy as np
import data 

class Database():
    def setup(self):
        # Instantiate database & generate table  
        print('Setting up database...')      
        self.db = dataset.connect('sqlite:///data.db')
        self.table = self.db.create_table('lunch')

        self.data = data.data 
        for d in self.data:
            self.table.update(d, ['type'])

        return self.db, self.table

    # Export the database for other use
    def export(self, table, fileName, formatType):
        freeze(table.all(), format='json', filename='lunch.json')

        
    # def AddEntries(dictionary):





