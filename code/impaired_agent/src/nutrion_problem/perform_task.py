'''
    Main file which performs the task at hand
    Currently, the task is to gather nutritional value of food items
    - decrease the fuzzy-ness of the fuzzy variables by tuning their membership function
    - information fuzziness is computed using the fuzzy entropy
    * Goal is to have membership functions approximate real values *
'''
# Import libraries
import numpy as np
import membership_functions
import database 

# Initialize & allocate
DataBase = database.Database()
db, table = DataBase.setup()

# Export
DataBase.export(table, 'lunch', 'json')

# Run
# if __name__ == "__main__":
    