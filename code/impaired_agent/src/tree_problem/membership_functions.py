class Membership():
    """
    Compute membership function:
    * each membership function reads from a `common sense` database which mimics NLP and an extensive knowledge database
    * each membership function applies the most recent `common sense` rules
    """
    def membership_pos(self, pos):
        return pos
 
    def membership_size(self, size):
        return size 

    def membership_col(self, color):
        return color 
