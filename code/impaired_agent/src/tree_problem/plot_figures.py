import numpy as np 
import matplotlib.pyplot as plt 

pos = ["left", "middle", "right"]
height = ["small", "kind of small", "kind of tall", "tall"]

def fuzzy_entropy(m, N):
    K = 1 #k_B*T, temperature constant
    # print(m,(1-m)*np.log(1-m))
    return -(1/N)*np.sum(np.nan_to_num(m*np.log(m) + (1-m)*np.log(1-m)))

def gauss(x, mean, std):
    t1 = x-mean
    # t1 = 0.5*np.ones(len(x))
    # temp = 0.15*np.ones(len(t1))
    temp = np.zeros(len(t1))
    t2 = std
    return np.maximum(temp, np.exp(-(t1/t2)**2))
    # return t1

def plot_initial(title, ax, nmeans):
    xmax = 10
    xmin = 0
    domain = np.arange(xmin, xmax, (xmax-xmin)/10)
    dm = (xmax-xmin)/(nmeans+1)
    std = 5-nmeans
    means = [dm+dm*i for i in range(0,nmeans)]
    f = np.empty((len(means),len(domain)))
    i = 0
    for m in means:
        f[i,:] = gauss(domain, m, std)
        ax.plot(domain, f[i,:])
        print(f[i,:])
        i+=1
    ax.set_xticklabels([])
    ax.set_title(title)
    ax.set_ylabel("$m$")
    ax.set_ylim([0,1.1])

def compute_entropy():
    xmax = 5
    xmin = 0 
    npoints = 500
    domain = np.arange(xmin,xmax,(xmax-xmin)/npoints)
    std = [0.5]
    mean = [1,3]
    membership = np.empty((len(std), len(domain)))
    fEntropy = np.empty(len(std))
    for i in range(0, len(std)):
        membership[i,:] = gauss(domain, mean, std[i])
        # print(membership[i,:])
        fEntropy[i] = fuzzy_entropy(membership[i,:], npoints)

    print(fEntropy)

# fig, ax = plt.subplots(1,2)
# plot_initial("height", ax[0], 4)
# plot_initial("positioning", ax[1], 3)


# plt.show()
compute_entropy()