'''
    Impaired robot;
    an impaired robot should generate common sense for a simple environment
'''
#Import
import numpy as np 
import membership_functions as mf

#Initialize experiment
primeGoal = ["position"]
subGoal = ["positioning", "size", "color"]
area = [0, 1]
n = 50 
m = 0.5
minFuzzy = 0.1 
boxWidth = 0.05
cost_increase = 1

######################
#Initialize functions#
######################
#Compute size of the box:
def size(x, sigma):
    return x + np.random.normal(x, sigma)

def search_box(box_pos):
    cost = 0
    boxNotFound = True
    boxMin = np.max(0, box_pos - 0.5*boxWidth)
    boxMax = np.min(box_pos + 0.5*boxWidth, 1)
    while boxNotFound:
        r = np.random.rand()
        if r > boxMin and r < boxMax:
            boxNotFound = False
            return box_pos 
        else:
            cost += cost_increase

def generate_common_sense():
    return mf.membership_col(1)


#Generate common sense database:
colors = {"red":mf.membership_col("red"), "blue":mf.membership_col("blue"), "green":mf.membership_col("green")}


if __name__ == "__main__":
    pos = generate_common_sense()
    print(pos)