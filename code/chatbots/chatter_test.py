from chatterbot import ChatBot

#Instantiate chatbot:
chatbot = ChatBot(
    "Chad Bodd",
    trainer = "chatterbot.trainers.ChatterBotCorpusTrainer",
    storage_adapter = "chatterbot.storage.SQLStorageAdapter",
    logic_adapters = [
        "chatterbot.logic.MathematicalEvaluation",
        "chatterbot.logic.TimeLogicAdapter",
        "chatterbot.logic.BestMatch"
    ],
    input_adapter = "chatterbot.input.TerminalAdapter",
    output_adapter = "chatterbot.output.TerminalAdapter",
    database = "../database.db"
)

#Teach the bot words based on the English corpus
chatbot.train("chatterbot.corpus.english")
#Export the learned trainer
chatbot.trainer.export_for_training("./trained_bots/corpus_english.json")

print("Hi I am Chad Bodd, let's talk!")

if __name__ == "__main__":
    while True:
        try:
            bot_input = chatbot.get_response(None)

        except (KeyboardInterrupt, EOFError, SystemExit):
            break


    