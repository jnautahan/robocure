import watson_developer_cloud
import setup 

convo = setup.SetupConversation()
conversation, workspace_id = convo.setup("nutrition")

#Initialize chat functionality:
user_input = ""
end_intent = ""
context = {}
entities = {

}

while end_intent != "end_conversation":
    #Send message to Watson:
    response = conversation.message(
        workspace_id = workspace_id,
        input = {
            "text" : user_input 
        },
        context = context
    )
    
    # TESTING MODE:
    # Print measured intents, entities and values
    if response['intents']:
        print("Detected intent: #" + response['intents'][0]['intent'])
    
    if response['entities']:
        new_entity = response['entities'][0]['entity']
        new_value = response['entities'][0]['value']
        entities.update({new_entity: new_value})
        print("Detected entity: @" + new_entity + "with value: " + new_value)
    
    #Print the response and let user input new message
    for text in response['output']['text']:
        print(text)

    context = response['context']
    user_input = input(">> ")