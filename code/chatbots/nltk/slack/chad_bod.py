'''
    Chad Bod -- The (hopefully) intelligent chatbot
    Integrating the bot to use in a Slack environment for testing and for deployment.
    Authentication for the Slack environment is done through the authentication.py file
    ---
    @TODO:
    - Generate database or use existing library for NLG or directed question asking
'''
import slackclient
import authentication
import time
import pprint
import sys 
sys.path.append("..")
import wordnet
wn = wordnet.WordNet()

# Authenticate:
slack_client, is_ok = authentication.authenticate()
print("Connection setup: " + str(is_ok))

# Set message posting:
def send_message(message, user, channel):
    post_message(message=message, channel=channel)

def post_message(message, channel):
    slack_client.api_call('chat.postMessage', channel=channel, text=message, as_user=True)

# Run the chatbot:
def run():
    ''' If SlackClient connects then print out to the `general` channel. 
    I currently have no idea to put this on a different channel, so `general for now`.
    * Get the message from Slack as the `message` part of the event
    * Use the message as input sentence for the wordnet.py code
    * Output the entity dictionary and send the message through Slack

    '''
    if slack_client.rtm_connect():
        print("Chad Bod is active!")
        send_message("Hello there! Please tell me what you ate today and I will try to parse the input to output the entities and their corresponding attributes!", authentication.BOT_ID, 'general')
        while True:
            event_list = slack_client.rtm_read()
            for event in event_list:
                if event['type'] == 'message' and event['user'] != authentication.BOT_ID:
                    text = event['text']
                    entities = wn.main(text)
                    message = entities
                    send_message(message, event.get('user'), event.get('channel'))
    else:
        print("Connection to Slack channel failed")
        exit 

if __name__ == "__main__":
    run()


