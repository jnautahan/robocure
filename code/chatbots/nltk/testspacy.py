# import nltk
# from nltk.corpus import wordnet
import spacy
import random
# from spacy.tokens import Span 
from spacy import displacy

# doc = nlp(u'Johannes is looking to use SpaCy instead of Stanford NLP and NLTK')

# for ent in doc.ents:
#     print(ent.text, ent.start_char, ent.end_char, ent.label_)

# displacy.serve(doc, style='ent')

nlp = spacy.load('en_core_web_sm')
sent = "I like to eat many big sandwiches"
doc = nlp(sent)

# TRAIN_DATA = [
#     ("I like sandwiches!", {'entities': [(7,17,'FOOD')]}),
#     ("I eat a couple sandwiches each day", {'entities': [(15,25, 'FOOD')]}),
#     ("Please give me a ham sandwich", {'entities': [(21,31, 'FOOD')]})
#     # ('Who is Shaka Khan?', {
#     #     'entities': [(7, 17, 'PERSON')]
#     # }),
#     # ('I like London and Berlin.', {
#     #     'entities': [(7, 13, 'LOC'), (18, 24, 'LOC')]
#     # })
# ]

# def main(n_iter):
#     nlp = spacy.blank('en_core_web_sm')  
#     ner = nlp.get_pipe('ner')
#     # Get names of other pipes to disable them during
#     other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']

#     # Add labels
#     for _, annotations in TRAIN_DATA:
#         for ent in annotations.get('entities'):
#             ner.add_label(ent[2])

#     # Only train 'NER'
#     with nlp.disable_pipes(*other_pipes):
#         optimizer = nlp.begin_training()
#         for i in range(n_iter):
#             random.shuffle(TRAIN_DATA)
#             losses = {}
#             for text, annotations in TRAIN_DATA:
#                 nlp.update([text], [annotations], drop=0.5, sgd=optimizer, losses=losses)
#             print(losses)

#     # Test trained model
#     for text, _ in TRAIN_DATA:
#         doc = nlp(text)
#         for ent in doc.ents:
#             print(ent.text, ent.label_)

lst = ['ADJ', 'NOUN', 'NUM']
posLst = []

for token in doc:
    if token.pos_ in lst:
        posLst.append(token.text)

for pos in posLst:
    for synset in wordnet.synsets(pos):
        print(synset)



