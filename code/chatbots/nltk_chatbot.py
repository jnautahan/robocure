#!/usr/bin/python3

#Import libraries
import numpy as np 
import nltk 

from nltk.chat.util import Chat, reflections

pairs = [
    [
        r"My name is (.*)",
        ["Hi %1! My name is Simple Chat, nice to meet you!"]
    ],
    [
        r"Hi",
        ["Hello!", "Hey, what's up?"]
    ],
    [
        r"quit",
        ["Ok, see you next time! Bye-bye!"]
    ]
]

def simple_chat():
    print("Hello, I am a simple chat bot! What is your name?")
    chat = Chat(pairs, reflections)
    chat.converse()

if __name__ == "__main__":
    simple_chat()