# def transcribe_file(speech_file):
#     from google.cloud import speech 
#     from google.cloud.speech import enums 
#     from google.cloud.speech import types
#     client = speech.SpeechClient()
import speech_recognition as sr 
from os import path 
AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "ENG_M.wav")

recognizer = sr.Recognizer()
with sr.AudioFile(AUDIO_FILE) as source:
    audio = recognizer.record(source)

IBM_USERNAME = "c4efb134-e701-4b66-8829-9eaf26d7b4b1"  # IBM Speech to Text usernames are strings of the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
IBM_PASSWORD = "nrrrxCVT8mBY"  # IBM Speech to Text passwords are mixed-case alphanumeric strings
try:
    print("IBM Speech to Text thinks you said " + recognizer.recognize_ibm(audio, username=IBM_USERNAME, password=IBM_PASSWORD))
except sr.UnknownValueError:
    print("IBM Speech to Text could not understand audio")
except sr.RequestError as e:
    print("Could not request results from IBM Speech to Text service; {0}".format(e))