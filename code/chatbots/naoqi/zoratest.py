# -*- encoding: UTF-8 -*-
'''
    Parse sentence whenever it hears a sentence
'''
from naoqi import ALProxy
import speech_recognition as sr 
import qi
import argparse
import functools
import sys
import get_entities

class ReactToSpeech(object):
    ''' A simple module able to react to touch events.
    '''
    def __init__(self, app):
        super(ReactToSpeech, self).__init__()
        self.app = app
        self.app.start()
        # # Get the services ALMemory, ALTextToSpeech, ALSpeechRecognition.
        session = app.session
        self.memory_service = session.service("ALMemory")
        self.stt = session.service("ALSpeechRecognition")
        # Subscribe to the SpeechToText service
        self.Name = "Test"
        self.stt.subscribe(self.Name)
        self.speechrecon = sr.Recognizer()
        self.tts = session.service("ALTextToSpeech")
        # Connect to an Naoqi1 Event
        self.speech = self.memory_service.subscriber("SpeechDetected")
        self.touch = self.memory_service.subscriber("TouchChanged")
        self.id = self.speech.signal.connect(functools.partial(self.speechDetected, "SpeechDetected"))
        self.touch_id = self.touch.signal.connect(functools.partial(self.closeOnTouch, "TouchChanged"))
        self.say("Zeg iets over eten")

    def speechDetected(self, eventName, value):
        if value == True:
            sentence = self.recognizeSpeech()
            parsed = self.parseSentence(sentence)
            return parsed

    # Listen to the microphone
    def recognizeSpeech(self):        
        self.speechrecon.dynamic_energy_threshold = True
        # self.speechrecon.energy_threshold = 300
        print("Recording audio...")
        with sr.Microphone() as source:
            audio = self.speechrecon.listen(source)
            print("Searching Google...")
        try:
            sentence = self.speechrecon.recognize_google(audio)
            return sentence
        except:
            print("Failed to recognize sentence")
            return 0

    # Parse sentence using WordNet and output entities
    def parseSentence(self, sentence):
        print("I heard: '%s'"%sentence)
        if sentence == "goodbye":
            print("wat")
            self.closeOnTouch(0,0)
        elif sentence:
            entities = get_entities.get_entities(sentence)
            for word, entity in entities.iteritems():
                for e in entity:
                    self.say(word)
        else:
            self.say("I heard nothing")


    # def onSpeech(self, strVarName, value):
    #     '''
    #         Say that he detected some speech
    #         word is given by getData as a string: "<...> word <...>", thus we use str(word[0][6:-6])) to get the full string
    #     '''
    #     # Disconnect to the event when talking, to avoid repetitions
    #     self.speech.signal.disconnect(self.id)

    #     word = self.memory_service.getData("WordRecognized")
    #     word = word[0][6:-6]   
    #     self.stt.unsubscribe(self.Name)        

    #     # Quit if it hears goodbye                     
    #     if word == "goodbye":
    #         self.close()

    #     # Say what he heard
    #     statement = "I heard " + word
    #     self.say(statement)

    #     # Reconnect to the event again
    #     self.id = self.speech.signal.connect(functools.partial(self.onSpeech, "WordRecognized"))        
    #     self.stt.subscribe(self.Name)

    def say(self, sentence):
        ''' Say the input sentence
        '''
        self.tts.say(sentence)

    # Unsubscribe from all when touched
    def closeOnTouch(self, strVarName, value):
        print("Closing down...")
        self.stt.unsubscribe(self.Name)
        self.touch.signal.disconnect(self.touch_id)
        self.app.stop()
        sys.exit(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="10.10.131.15",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["ReactToSpeech", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    # react_to_touch = ReactToTouch(app)
    react_to_speech = ReactToSpeech(app)
    app.run()