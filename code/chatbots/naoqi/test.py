import speech_recognition as sr 

# Record audio from the microphone
r = sr.Recognizer()
with sr.Microphone() as source:
    print("Calibrating microphone")
    r.adjust_for_ambient_noise(source, duration=5)
    r.dynamic_energy_threshold = True
    print("Say something")
    audio = r.listen(source)

sentence = r.recognize_google(audio)
print(sentence)