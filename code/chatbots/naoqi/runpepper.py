from __future__ import print_function
# import naoqi
# gatherer = get_entities.get_entities()
from naoqi import ALProxy

import get_entities
import qi 
import argparse, sys, time

IP = "10.10.130.176"
# asr = ALProxy("ALSpeechRecognition", IP, 9559)
# asr.setLanguage("English")
def load_dict():
    f = open("british-english")
    words = f.read()
    f.close()
    words = list(words.split())
    return words


def main(session):
    # Get the service ALSpeechRecognition.    
    asr_service = session.service("ALSpeechRecognition")
    text2speech = session.service("ALTextToSpeech")
    text2speech.say("Hello!")

    asr_service.setLanguage("English")

    # Example: Adds "yes", "no" and "please" to the vocabulary (without wordspotting)
    # vocabulary = ["yes", "no", "please"]
    vocabulary = load_dict()
    asr_service.setVocabulary(vocabulary, True)

    # Start the speech recognition engine with user Test_ASR
    asr_service.subscribe("Speech_recognition")
    print("Speech recognition engine started")
    asr_service.unsubscribe("Speech_recognition")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default=IP, help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559, help="Naoqi port number")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session)

