'''
    Calls wordnet.py to gather entities from a parsed sentence
'''
#!/usr/bin/python2.7
from __future__ import print_function
import wordnet 
import pprint

# Instantiate wordnet class:
wn = wordnet.WordNet()

# Get entities
def get_entities(sentence):
    entities = wn.main(sentence)
    return entities 
    