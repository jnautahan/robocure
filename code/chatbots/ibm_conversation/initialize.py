import json
import sys
import watson_developer_cloud

#Conversation class:
class constructConversation:
    def initialize(self, workspace_name):
        
        conversation = watson_developer_cloud.ConversationV1(
            username = 'b49c7313-8fa7-4a79-a519-7530e619ea4f',
            password = 'VNwJs1jIpA5e',
            version = '2017-05-26'
        )

        #Instantiate workspace
        if workspace_name:
            workspace_list =  conversation.list_workspaces()["workspaces"]
            workspace_exists = False

            #Load workspace if it exists:
            for workspace in workspace_list:
                if workspace["name"] == workspace_name:
                    print("Using workspace: [" + workspace_name +"]")
                    workspace_exists = True
                    workspace_id = workspace["workspace_id"]
                    break
            
            #Otherwise create new workspace
            # * user is askes for confirmation since the # of workspaces is limited
            if not workspace_exists:
                #List all current available workspaces:
                print("Currently the following workspaces are available:")
                for workspace in workspace_list:
                    print(workspace["name"])
                print("Currently the requested workspace [" + workspace_name + "] is not available, do you wish to instantiate it?")
                uin = input("Confirm with yes/no: ")
                if uin == "y" or uin == "yes":
                    print("Creating workspace")
                    workspace = conversation.create_workspace(name = workspace_name)
                    workspace_id = workspace["workspace_id"]
                else:
                    print("Please correct the name of the workspace and re-run")
    
        else:
            "Please provide a name for a workspace"
            exit()
    
        if workspace_id:
            return conversation, workspace_id
    
