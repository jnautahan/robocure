import json
import watson_developer_cloud

conversation = watson_developer_cloud.ConversationV1(
    username = 'b49c7313-8fa7-4a79-a519-7530e619ea4f',
    password = 'VNwJs1jIpA5e',
    version = '2017-05-26'
)
workspace_id = '77c5169d-b84e-4403-ba54-2e292fd87e95'

#Initialize:
user_input = ''
current_action = ''
context = {}

while current_action != 'end_conversation':
#Send message to Watson Conversation servie:
    response = conversation.message(
        workspace_id = workspace_id,
        input = {
            'text': user_input
        },
        context = context 
    )

    if response['output']['text']:
        print(response['output']['text'][0])

    #Update the stored context:
    if response['intents']:
        current_intent = response['intents'][0]['intent']
        print('Detected intent: #' + current_intent)
    #Check for action flags:
    if 'action' in response['output']:
        current_action = response['output']['action']
        print(current_action)
    if current_action != 'end_conversation':
        user_input = input('>> ')