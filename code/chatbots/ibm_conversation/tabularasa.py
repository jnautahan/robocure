'''
    Tabula Rasa class:
    Instantiate class where tabula rasa learning can take place
'''
import json
import sys
import watson_developer_cloud

#Conversation class:
class Tabula:
    def initialize(self, workspace_name):
        
        conversation = watson_developer_cloud.ConversationV1(
            username = 'b49c7313-8fa7-4a79-a519-7530e619ea4f',
            password = 'VNwJs1jIpA5e',
            version = '2017-05-26'
        )

        #Instantiate workspace
        if workspace_name:
            workspace_list =  conversation.list_workspaces()["workspaces"]
            workspace_exists = False

            #Load workspace if it exists:
            for workspace in workspace_list:
                if workspace["name"] == workspace_name:
                    print("Using workspace: [" + workspace_name +"]")
                    workspace_exists = True
                    workspace_id = workspace["workspace_id"]
                    break
            
            #Otherwise create new workspace
            # * user is askes for confirmation since the # of workspaces is limited
            if not workspace_exists:
                #List all current available workspaces:
                print("Currently the following workspaces are available:")
                for workspace in workspace_list:
                    print(workspace["name"])
                print("Currently the requested workspace [" + workspace_name + "] is not available, do you wish to instantiate it?")
                uin = input("Confirm with yes/no: ")
                if uin == "y" or uin == "yes":
                    print("Creating workspace")
                    workspace = conversation.create_workspace(name = workspace_name)
                    workspace_id = workspace["workspace_id"]
                else:
                    print("Please correct the name of the workspace and re-run")
    
        else:
            "Please provide a name for a workspace"
            exit()
    
        if workspace_id:
            return conversation, workspace_id
    
    #Add new intent if it does not exist already:
    def add_new_intent(self, conversation, workspace_id):
        #Check if new intent exists in the full list of intents:
        print("No corresponding intent, please add as a new intent:")
        newintent = input("New intent: ")
        if not self.intent_exists(conversation, workspace_id, newintent):
            #Add intent:
            conversation.create_intent(
                workspace_id = workspace_id,
                intent = newintent
            )
            #Add examples to the new intent:
            examples = []
            exampleStr = ""
            while exampleStr is not "0":
                if exampleStr == "0":
                    break 
                exampleStr = input("Example: ")
                examples.append({"text": exampleStr})
            conversation.update_intent(
                workspace_id = workspace_id,
                intent = newintent,
                new_examples = examples 
            )
        else:
            print("Intent already exists")

    #First generate list of available intents, if the newintent is not in this list, add it and return False:
    def intent_exists(self, conversation, workspace_id, newintent):
        try:
            intents_list
        except:
            intents_list = []
            current_intents = conversation.list_intents(
                workspace_id = workspace_id
            )
            for intent in current_intents["intents"]:
                intents_list.append(intent["intent"])
        #Check if the new intent is in the list       
        if newintent not in intents_list:
            intents_list.append(newintent)
            return False
        else:
            return True

    #Generate dialogue
    def add_new_dialogue(self, conversation, workspace_id):
        return 0
