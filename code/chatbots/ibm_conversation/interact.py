import json
import tabularasa

#Initialize the conversation object:
workspace_name = "tabula-conversation-test"
#Create workspace (if needed) and get conversation object and workspace_id:
tabula = tabularasa.Tabula()
conversation, workspace_id = tabula.initialize(workspace_name)

#Initialize chat functionality:
user_input = ""
end_intent = ""

#The chatbot:
while end_intent != "end_conversation":
    #Send message to Watson:
    response = conversation.message(
        workspace_id = workspace_id,
        input = {
            "text" : user_input 
        }
    )
    if not response["intents"]:
        tabula.add_new_intent(conversation, workspace_id)
    user_input = input(">> ")

