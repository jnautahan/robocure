import spacy 
nlp = spacy.load('nl')
txt = nlp(u'De witte kool is sowieso aan gast')



# # for chunk in txt.noun_chunks:
# #     chunk.merge(tag=chunk.root.tag_, lemma=chunk.root.lemma_, ent_type=chunk.root.ent_type_)

index = None
indexLst = []
for token in txt:
    print(token.dep_)
    if token.dep_ == "compound" or token.dep_ == "amod":
        index = token.idx if not index else index 
    else:
        if index:
            indexLst.append([index, token.idx+len(token)])
            index = None 

# Merge 
for locs in indexLst:
    txt.merge(locs[0], locs[1])

for tok in txt:
    print(tok.text, tok.pos_)

spacy.displacy.serve(txt)