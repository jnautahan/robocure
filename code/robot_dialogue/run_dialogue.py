'''
    Top-level executable which communicates with all the modules and activates them if needed.
    This file will:
    * establish connection with the robot   [connect.py]
    * close connection with the robot       [connect.py]
    * activate speech to text module        [speechtotext.py]
    * parse sentences using SpaCy/Wordnet   [parser.py]
    * gather entities and keywords          [get_entities.py, parser.py]
    * decide on proper reply given input    [dialogue.py]
    * control the flow of the dialogue      [dialogue.py]

    ~~~
    The goal is that this file is an executable which connects all the modules and 'activates' them, then let them do the work until the conversation is over. Therefore it acts as the top-level 'manager'.
    ~~~
'''
# Append paths where modules are
import sys, os, operator
# current_dir = os.getcwd()
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir+"/src/"); 
sys.path.append(current_dir+"/data/") 
# Import necessary libraries and modules
import connect, speechtotext, texttospeech, dialogue, database
import yaml, langdetect

# Gather necessary parameters
f = current_dir+"/params.yaml"
with open(f) as stream:
    try: 
        params = yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        sys.exit(1)

class Manager(object):
    ''' Manager class which holds all class objects from the modules which are imported. Each module is a seperate manager and is responsible for a small subset of tasks and can be called by the parent Manager class.
        Managers should be self-explanatory due to their names
    '''
    # Let constructer know all necessary parameters
    def __init__(self, params):
        self.params = params
        self.ip_address = self.params['ip_address']
        self.port_nr = self.params['default_NAOqi_port_nr']
        self.work_dir = self.params['work_directory']
        self.data_dir = self.params['data_directory']
        self.audio_file = self.params['audio_file']
        self.nao_audio_file = self.params['audio_file_on_nao']
        self.sentences_file = self.params['sentences_file']
        self.language = self.params['language']
        self.text_input_file = self.params['test_text_input']%(self.language)
        self.APIservice = self.params['APIservice']
        self.audio_from_data = self.params['load_audio_from_data']
        self.text_input = self.params['text_input']
        self.recording = self.params['only_record_samples']
        self.connection_with_robot = not self.audio_from_data and not self.text_input
        self.dialog_params = [None]
        self.full_audio_text = ""
        
        # Make all the managers
        if self.connection_with_robot:
            if self.recording:
                self.recording_dir = self.params['test_directory']
            self.ConnectionManager = connect.Connect(self.ip_address, self.port_nr)
            try:
                self.SpeechToTextManager = speechtotext.SpeechToText(self.ConnectionManager.session,  self.audio_from_data, self.recording, self.text_input, self.ip_address, self.work_dir, self.audio_file, self.nao_audio_file, self.text_input_file, self.language)
            except:
                self.SpeechToTextManager = speechtotext.SpeechToText(self.ConnectionManager.session, self.audio_from_data, self.recording, self.text_input, self.ip_address, self.recording_dir, self.audio_file, self.nao_audio_file, self.text_input_file, self.language)
            self.TextToSpeechManager = texttospeech.TextToSpeech(self.audio_from_data, self.ConnectionManager.session, self.language)
            #@TODO: Once these values come in through IoT, feed it this
            self.plate = self.params['test_plate']%(self.language)
        else:
            self.test_dir = self.params['test_directory']
            self.plate = self.params['test_plate']%(self.language)
            session = ""
            self.SpeechToTextManager = speechtotext.SpeechToText(session, self.audio_from_data, self.recording, self.text_input, self.ip_address, self.test_dir, self.audio_file, self.nao_audio_file, self.text_input_file, self.language)
            self.TextToSpeechManager = texttospeech.TextToSpeech(not self.audio_from_data, session="", language = self.language)
        self.SentenceManager = dialogue.Sentences(self.sentences_file, self.language)    
        self.DialogueManager = dialogue.Dialogue(self.language, self.plate)

        # Get a dictionary which includes all the phases with corresponding topics organized as: {phase_i: [topic_list_i]
        # The corresponding topic_list is ordered according to the rank of the topics
        self.topic_dict = {}
        for topic in self.SentenceManager.sentences:
            struct = self.SentenceManager.sentences[topic]
            phase = struct["phase"]
            # Phase -1 indicates 'filler' sentences, such as "yes", "no"
            if phase is not -1:
                try:
                    rank = struct["rank"]
                    try:
                        self.topic_dict[phase][rank] = topic
                    except:
                        try:
                            self.topic_dict[phase].extend([None]*rank)
                        except:
                            self.topic_dict[phase] = [None]*(rank+1)
                        self.topic_dict[phase][rank] = topic
                except:
                    self.topic_dict[phase] = [topic]
            else:
                try:
                    self.topic_dict[phase].append(topic)
                except:
                    self.topic_dict[phase] = [topic]
        # print(self.topic_dict)
        # Get maximal phase, i.e. stopping criterion for the dialogue
        self.max_phase = max(self.topic_dict.keys())
    
    def SpeechToText(self):
        ''' Module which calls the SpeechToTextManager 
            Gets audio as input and gives text as output
            Detect language from the string. If the string is too short, language recognition is too imprecise and should not be used in order to avoid unwanted switches.
        '''
        text = self.SpeechToTextManager.GetText(self.APIservice, self.audio_from_data, self.text_input)
        n_words = len(text.split())
        if text and n_words > 4:
            for lang in langdetect.detect_langs(text):
                lang = str(lang)    # Convert langdetect Language object to string
                lang, confidence = lang.split(":")
                if confidence > 0.95:
                    # Get new text or stay if language is the current language
                    self.SwitchLanguage(lang)
        return text

    def SwitchLanguage(self, language):
        ''' Switch into the recognized language if it is not the current one '''
        # Transform langdetect language strings to our case:
        if language == 'nl':
            language = "dutch"
        elif language == 'en':
            language = "english"
        elif language == 'fr':
            language = "french"
        else:
            language = ""
        # Switch languages, i.e. call SwitchLanguage method in all relevant managers
        if language and not language==self.language:
            self.language = language
            self.TextToSpeechManager.SwitchLanguage(self.language)
            self.SpeechToTextManager.SwitchLanguage(self.language)
            self.DialogueManager.SwitchLanguage(self.language)
            self.SentenceManager.SwitchLanguage(self.language)
            # Utter something in the new language, indicating the change
            sent, cont, add_var = self.SentenceManager.SelectSentence("switch_language")
            self.TextToSpeechManager.say(sent, self.connection_with_robot, "")

    def Greeting(self):
        ''' Module which calls DialogueManager
            Only for initial greetings, afterwards the flow will update such that we arrive in phase 1 where the `real` conversation begins
        '''
        # topics = self.DialogueManager.DetermineTopics(self.topic_dict)
        # for topic in topics:
        #     self.Speak(topic, var="")
        self.DialogueManager.NextPhase()
        return self.DialogueManager.PrepareNextPhase(self.dialog_params)

    def Speak(self, topic, var):
        ''' Speak sentences within a topic
        '''
        sent, cont, add_var = self.SentenceManager.SelectSentence(topic)
        self.TextToSpeechManager.say(sent, self.connection_with_robot, var)
        self.DialogueManager.UpdateFlow(cont, self.DialogueManager.next_phase)

    def Listen(self, param):
        ''' Listen to audio and gather the topic
            If the language is English, one *can* swap to the Wordnet variant. However note that this is not necessary
        '''
        text = self.SpeechToText()
        # print("Understood sentence: ", text)
        # If entity recognition fails, immediatly return the "unclear" topic
        if text is not None:
            # entities = self.DialogueManager.GetEntities(text)
            entities = self.DialogueManager.GetPOSTags(text, self.language)
            return self.DialogueManager.Analyze(text, entities, param)
        else:
            return "unclear", False
    
    def SubListen(self, param, depth):
        ''' Listen to audio and gather subtopics
            If the language is English, one *can* swap to the Wordnet variant. However note that this is not necessary
        '''
        text = self.SpeechToText()
        print("Understood sentence: ", text)
        # If entity recognition fails, immediatly return the "unclear" topic
        try:
            # entities = self.DialogueManager.GetEntities(text)
            entities = self.DialogueManager.GetPOSTags(text, self.language)
            return self.DialogueManager.AnalyzeSubDialogue(text, entities, param, depth)
        except:
            return "unclear", False

    def Dialogue(self, topics, param):
        ''' Does a dialogue for every phase. 
            Dialogue ends when the output of the dialogue class gives a boolean equal to True. In phase 2, a subdialogue is triggered which is unique to this phase. 
            General structure is as follows:
            for a certain phase:
                say sentence if question
                listen and analyze response
                respond
                repeat or continue until done
        '''
        cur_phase = self.DialogueManager.phase
        # In the first phase we only need all the food items and if they contain carbs or not
        if cur_phase == 1:
            topic = topics[0] if type(topics) == list else topics
            if self.SentenceManager.sentences[topic]["question"]:
                self.Speak(topic, var="")
            topic, next_phase = self.Listen(param="")
            if not self.SentenceManager.sentences[topic]["question"]:
                self.Speak(topic, var="")
            if not next_phase:
                return self.Dialogue("what_else", param)
            else:
                # print("Current known param: " + str(param))
                self.Speak("done", var="")
                param = self.DialogueManager.PrepareNextPhase(param)
                self.DialogueManager.NextPhase()
                return param
        
        # Sub dialogue only necessary for gathering the nutritional value of each fooditem that contains carbohydrates
        if cur_phase == 2:
            for p in param:
                # Only do the dialogue for food that contain carbs
                if True in p:
                    next_param = self.SubDialogue(p, topics, depth=0)
            self.DialogueManager.PrepareNextPhase(param)
            self.DialogueManager.NextPhase()
            # print(self.DialogueManager.GroundTruth.subjective_nutrition)
            self.Speak("done", var="")
            return param 

        # Final phase, special case
        if cur_phase == 5:
            ''' Finish up '''
            self.Speak("finish", var="")
            self.DialogueManager.NextPhase()
            return param

        # All other phases are similar
        else:
            topic = topics[0] if type(topics) == list else topics
            if self.SentenceManager.sentences[topic]["question"]:
                self.Speak(topic, var="")
            topic, next_phase = self.Listen(param="")
            if not self.SentenceManager.sentences[topic]["question"]:
                self.Speak(topic, var="")
            if not next_phase:
                self.Dialogue("disagree", param)
            else:
                self.DialogueManager.NextPhase()
                return self.DialogueManager.PrepareNextPhase(param)            

    def SubDialogue(self, param, topics, depth):
        ''' Performs subdialogue which repeats a certain dialogue multiple times
            In this case, this is done to get the nutritional value of every food item on the plate
            param[0] holds the variable name
        '''
        try:
            topic = topics[depth]
        except:
            topic = topics[-1]
        # Utter sentence if it is a question
        if self.SentenceManager.sentences[topic]["question"]:
            self.Speak(topic, param[0])
        # Listen, analyze and respond:
        topic_, next_param, next_depth = self.SubListen(param[0], depth)
        self.Speak(topic_, var="")
        if not self.SentenceManager.sentences[topic]["question"]:
            self.Speak(topic, param[0])
        # Make recurrent call to SubDialogue if it is not finished
        if not next_param:
            if not next_depth:
                return self.SubDialogue(param, topics, depth)
            if next_depth:
                return self.SubDialogue(param, topics, depth+1)
        else:
            return next_param

    def ShutDown(self, value):
        ''' End the program '''
        if not value:
            self.ConnectionManager.Close()
        sys.exit(1)


if __name__ == "__main__":
    ''' Run the dialogue framework
        First instantiate the Manager class with the correct parameter file and greet the patient. Then if not testing and not recording, start the dialogue framework. If testing, the output is printed to the terminal instead of letting the robot say it. If recording, the robot does not say anything, but N audio files can be recorded.
    '''
    # Instantiate the Manager class and its manager modules
    Manager = Manager(params)

    # Run initial phase as introduction:
    Manager.dialog_params = Manager.Greeting()
    max_flow = (Manager.max_phase-1)*Manager.DialogueManager.delta_flow
    # If you only record samples, no need to instantiate Dialog framework and such, just listen and save
    if Manager.recording:
        for i in range(0, 10):
            Manager.Listen()
    else:
        # Perform dialogue until finished
        while Manager.DialogueManager.flow <= max_flow:
            topic = Manager.DialogueManager.DetermineTopics(Manager.topic_dict)
            Manager.dialog_params = Manager.Dialogue(topic, Manager.dialog_params)

    # Close connection
    if Manager.audio_from_data or Manager.text_input:
        Manager.ShutDown(True)
    else:
        Manager.ShutDown(False)
    