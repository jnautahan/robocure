# Dialogue system for NAOqi agent for aiding diabetic children
The code generate a dialogue system which is used in any robot which used the NAOqi framework, such as Pepper and Zorabot. Code is mostly written in Python and uses `Python 2.7.x` since the NAOqi framework requires this version. Code is commented thoroughly but please let me know if anything is unclear. If everything works, using this is just executing a single file. All seperate modules have introductory comments if one wishes to know more about them.

## Installation
All necessary libraries listed below need to be installed before the program can be ran. I personally recommend using a [virtual environment](http://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html) and installing all necessary libraries through `pip`. See the listed links for more information about specific libraries.

### Required software & libraries
* Python 2.7.x
* Python json; `import json`, used for loading the sentences
* [SpeechRecognition 3.8.1](https://pypi.python.org/pypi/SpeechRecognition/); `import speech_recognition as sr`, used for speech recognition using the (free) Google API. One can also directly use any API one desired, however this library has support for multiple APIs, both online and offline.
* [SpaCy 2.x.x](https://spacy.io/); `import spacy`, used for Part of Speech (POS) tagging. This is used instead of NLTK since SpaCy offers support for the Dutch and French language in addition to English.    
**NOTE** Language models should be loaded _beforehand_, see *POS Tagging with SpaCy* section
* [naoqi](http://doc.aldebaran.com/2-5/dev/python/install_guide.html); `import naoqi` or `from naoqi import ALProxy`, used for communication between code and the NAOqi bot(s). When using the NAOqi framework, do not forget to export the correct pythonpath: `export PYTHONPATH=~/path/to/py-naoqi/lib/python2.7/site-packages/`!
* [langdetect](https://pypi.python.org/pypi/langdetect); used for language detection using the plain text gotten from the API. If the detected language is not the same as the current language, one can swap into a different language in the middle of a dialogue.

### APIs
* [Google Cloud Speech API](https://cloud.google.com/speech/docs/reference/libraries); used for speech recognition within the SpeechRecognition 3.8.1 library. Supports multiple languages. Needs credentials listed in JSON format, see `api_key.json` for more information. 
* [IBM Speech to Text](https://www.ibm.com/watson/services/speech-to-text/); used for speech recognition within the SpeechRecognition 3.8.1 library. Currently only supports English in this case. Credentials are hard-coded.

All other libraries should be included in the standard Python distribution. If not, install them through pip or your favourite method.

### Recommended software & libraries
* Virtual Python Environment (either [virtualenv](https://pypi.python.org/pypi/virtualenv) or the wrapper [virtualenwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/), I personally advocate for the second), used for managing development workflow and easier working on multiple projects without introducing conflicts in their dependencies

## Directory structure
```
.
├── data
│   ├── api_key.json
│   ├── audiofiles
│   │   ├── audio.wav
│   │   ├── example_dialogue
│   │   │   ├── example1
│   │   │   ├── example2
│   │   │   ├── exampledutch
│   │   │   └── examplefrench
│   ├── dinner_plate_dutch.json
│   ├── dinner_plate_english.json
│   ├── dinner_plate_french.json
│   ├── sentences.json
│   └── textfiles
│       ├── example_dutch.txt
│       ├── example_english.txt
│       └── example_french.txt
├── params.yaml
├── README.md
├── run_dialogue.py
└── src
    ├── connect.py
    ├── database.py
    ├── dialogue.py
    ├── lang_parser.py
    ├── spacytest.py
    ├── speechtotext.py
    ├── texttospeech.py

```


Upper directory contains the following files:
* `run_dialogue.py` contains the executable which manages all submodules and produces the dialog
* `params.yaml` contains all parameters in the YAML format. This allows a user to make easy changes to the workings of the software such as the language or directories where files are stored.
* `README.md` is this README file

`/data/` contains the data files of the following things:
* `api_key.json` holds the API Credentials for the used Google Cloud Speech API. This file is not in the repo since it is personal for each user. It is highlighted here since any user does need this if it wants to use the Google Cloud Speech API. Please note that one has to `export GOOGLE_APPLICATION_CREDENTIALS="~/location/of/api_key.json"` such that the API can find the file containing the credentials. See [here](https://cloud.google.com/speech/docs/reference/libraries) for more info.
* `dinner_plate_[LANGUAGE].json` holds the contents of the dinner plate in various languages, i.e. the ground truth
* `sentences.json` holds all possible sentences which the agent can say in a JSON format. More explanation about the format can be found in `dialogue.py`.
* Nutritional database: Python scripts to get and save data from a private folder. This data is not included in this directory since it cannot be distributed freely. The database contains data of many food items found in the supermarket. 
* `/audio_files/` holds all audio samples to be used for testing
* `/textfiles/` holds all text files to be used for testing

`/docs/` contains the documentation (Note: documentation might be limited to this README, in which case the /docs/ directory will be removed)

`/src/` contains all submodules which do the following:
* `connect.py` makes the connection with the robot, subscribes to the right events and disconnects once everything is finished
* `database.py` loads and manages the ground truth data and has methods which allows comparison between the subjective and the ground truth
* `dialogue.py` contains the flow parameters and what to do (i.e. what to say/respond) in what phase. 
* `speechtotext.py` is the main module which converts the speech from the patient to a string which can be parsed. Conversion is done by an API and using the `speech_recognition` Python library.
* `texttospeech.py` is the module which makes the agent say the desired sentences
* `parser.py` parses the sentence and gathers POS tags. Can also search [WordNet](http://wordnetweb.princeton.edu/perl/webwn) for entities using tokens from a parsed sentence if one desires, however this only works for the English language. Parsing is done by either SpaCy or NLTK, however SpaCy provides support for more languages and is thus the default.


## Usage
In this section I will (briefly) explain how to use the code and what simple adaption can be made in order to use the dialog for example in multiple languages.

### Basic execution
Fill in the `params.yaml` file to your liking and specify the directories and language. Please ensure that the directories exist. The following parameters are test parameters: 
```
    load_audio_from_data:   True/False  |   Load audio from files in specified directory   
    text_input:             True/False  |   Hold conversation by typing in terminal
    only_record_samples:    True/False  |   Only listen and save the samples, so that later
                                            load_audio_from_data can be called
```
If all are set to `False`, the program will execute such that a dialogue can be performed with the robot. 
After the parameters are filled in correctly, run `python run_dialogue.py`. Please note that 

### Connection
Connection is made to the IP address specified by the robot. Fill this one in in the parameter file. The `default_NAOqi_port_nr` defaults to `9559`, change if necessary. The `audio_file_on_nao` is a directory _on_ the robot itself. 

### API Services
Currently the following API Services are supported:
* `"Google"`: Google Cloud Speech (payed)
* `"FreeGoogle"`: Google Speech (free)
* `"IBM"`: IBM Speech to Text (free)

Names in the parameter file should be one of the names listed above.

### POS Tagging with SpaCy
First, the models need to be loaded. This can be done preemptively by the following command:
``` 
    python -m spacy download [LANGUAGEMODEL]
```
The necessary language models are: `en` for English, `nl` for Dutch and `fr` for French (see for more info [here](https://spacy.io/usage/models)). All three can be loaded such that swapping between them is fast.