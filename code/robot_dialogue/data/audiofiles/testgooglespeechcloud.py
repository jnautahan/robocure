import io, os
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types 

audio = os.path.join(os.path.dirname(__file__), 'sampleEng.wav')

google_client = speech.SpeechClient()
with io.open(audio, 'rb') as audio_file:
    content = audio_file.read()
    audio = types.RecognitionAudio(content=content)
config = types.RecognitionConfig(
    encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
    sample_rate_hertz = 48000,
    language_code='en-US'
)
response = google_client.recognize(config, audio)
for result in response.results:
    reply = result.alternatives[0].transcript
    print("Transcript: {}".format(reply))