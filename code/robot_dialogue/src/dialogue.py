'''
    Generate dialogue based on real-world examples. In certain situations, certain questions should be asked accordingly. The goal of this dialogue is to have an educational process in which the patient determines the nutritional value of the meal he/she is about to eat. 
    Supported languages should be: Dutch, English, French

    Example case:
    Q: What is on the plate?
    [ Contents of the plate are known by the agent. The goal is to have the patient guess the contents approximately such that the number of insulin units can be computed and applied. ]
    {A: The agent; P: The patient}
    A:  What items on the plate hold carbohydrates?
    P:  The chicken!
    A:  No, I am afraid that is wrong.
    P:  The potatoes
    A:  Yes, that is correct. What other food item contains carbs?
    P:  It's not in the greens
    A:  That's right. What else?
    P:  The yoghurt.
    A:  Yes, that's true. Any other?
    A:  That's correct. So how much of the potatoes are you going to eat? Please weight the amount using the scale.
    P:  I think I'll eat this much, which is 200 grams
    A:  That's right. And how much does the yoghurt weigh?
    P:  The yoghurt weighs 225 grams
    A:  And how many carbs per 100 gram?
    P:  It contains 18 grams of carbs per 100 grams
    A:  So what is the total amount or carbohydrates?
    P:  So the total amount of carbs in both the potatoes and yoghurt is around 75 grams.
    A:  That's right. So let's compute how much insulin you have to take. Please look up in the booklet how much you need.
    P:  It says I need around 5 units of insulin.
    A:  Yes, that's right! So, please take your insulin and enjoy your dinner!
'''
# Import necessary libraries and/or scripts
import json
import lang_parser, database
import numpy as np

class Sentences():
    ''' Gets the sentences from the corresponding datafile
        Sentences should be in json format as follows:
        "topic": {
            "response": [
                {
                    "sentence": "This is an example",
                    "language": "english"
                },
                {
                    "sentence": "Dit is een voorbeeld",
                    "language": "dutch
                }
            ],
            "continue": boolean,
            "addvariable": boolean,
            "phase" : integer,
            "rank": integer,
            "question": boolean
        }
        
        The "topic" defines what the sentences are about, such as a greeting.
        Inside the "topic" are the "responses" which hold the utterances in various languages.
        The "continue" variable is a boolean which indicates that the phase is finished and that the next phase can be initiated
        The "addvariable" variable is a boolean which indicates that a variable should be included in the response. As an example, parts of the (previously) given input can be included such as names of persons or objects. [Example; Yes {Mike}, that is true!] The place of the variable should be indicated by %s since this can then be called in the code for filling in a variable.
        The "phase" variable indicates to which phase of the dialogue the responses belong. One can use this integer to select responses from the JSON file.
        The "rank" variable indicates the rank if and only if there are multiple topics within the same phase. Then the rank indicates in what order the topics should be discussed.
        The "question" variable indicates if a response is a question or not. If so, the Dialogue framework can utter the response *before* waiting for an answer, while if false the Dialogue framework waits until an answer and *afterwards* generates a response.
    '''
    def __init__(self, path_to_file, language):
        self.sentences = json.load(open(path_to_file))
        self.topics = self.sentences.keys()
        self.language = language

    def SwitchLanguage(self, language):
        self.language = language
            
    def SelectSentence(self, topic):
        ''' Module which gets all possible sentences for a certain topic and language
        ''' 
        # Gather sentences for the topic and only return one of the variations of the given language
        temp_sentences = self.sentences[topic]["response"]
        sentences = []
        for sent in temp_sentences:
            if sent["language"] == self.language:
                sentences.append(sent["sentence"])

        return sentences[np.random.randint(0,len(sentences))], self.sentences[topic]["continue"], self.sentences[topic]["addvariable"]
        

class Dialogue():
    ''' Holds the main structure of the dialogue flow
        Flow parameter should increase each time the dialogue is to proceed to the next phase. However, in order to get a measure of the time it needs to we define the flow parameter as follows:
        * Flow parameter starts at 0
        * For every utterance the agent makes, the flow parameter increases by 1
        * When a phase is completed, the flow parameter is increased to the nearest number equal to F, i.e. 
            flow <- flow + F - (flow % F)
          this is done to allow for intermediate measures of the conversation flow. Then phase is computed as:
            phase <- flow/delta_flow
        * Phases of the dialogue are defined by topics which are dependent on the context. Example topics can be: greetings, asking_name and asking_amount_insulin
    '''
    def __init__(self, language, plate):
        # Initialize flow parameters
        self.flow = 0                                   #Initialize flow
        self.delta_flow = 1000                          #
        self.phase = int(np.floor(self.flow/self.delta_flow))             
        self.language = language                        #To be spoken language
        self.plate = plate                              #JSON encoded file with dinner plate info
        self.GroundTruth = database.Parameters(plate)   #Get class which holds ground truth values
        self.food_with_carbs = []                       #Contains list with foods that contain carbs
        self.next_phase = False                         #If true, proceed to next phase
        self.amounts = {}                               #Holds the amount to be eaten in [units]
        self.count = 0
        self.rank = -1
        self.yesno = ["disagree", "affirm"]
        self.denial_list = ["no", "not", "none", "nee", "geen", "niet", "non", "ne pas", "pas"]
        self.subjective_truth = ()
        self.Wordnet = lang_parser.Parser(language)

        # Define some functions
        self.flatten = lambda l: [item for sublist in l for item in sublist]

    def SwitchLanguage(self, language):
        self.language = language

    def UpdateFlow(self, cont, next_phase):
        ''' Update the flow parameter
            cont == True indicates that the next phase can be started
        '''
        if next_phase:
            self.flow = self.flow - (self.flow%self.delta_flow) + self.delta_flow
            print("-- Phase %s --"%(self.phase+1))
        elif cont:
            self.flow = self.flow + 1
        
    def NextPhase(self):
        ''' Proceed to next phase and reset necessary parameters '''
        self.next_phase = True
        self.phase = int(np.floor(self.flow/self.delta_flow))
        self.UpdateFlow(True, self.next_phase)
        self.next_phase = False
        self.rank = -1

    def DetermineTopics(self, topic_dict):
        self.rank += 1
        self.phase = int(np.floor(self.flow/self.delta_flow))
        return topic_dict[self.phase]            

    def PrepareNextPhase(self, phase_input):
        ''' Module which prepares the next phase by giving a list of parameters which can be iterated through. The length of the list triggers that many subdialogues. Most of the time, the output of the previous phase is used as input for the next phase
        '''
        if self.phase == 1:
            ''' Return list of food items on the plate and if they contain carbs or not '''
            # Get ground truth from the Parameter class in database.py 
            self.ground_truth = self.GroundTruth.dinner_plate
            return self.ground_truth
        
        else:
            ''' Give output from previous phase as input '''
            return phase_input

    def Analyze(self, text, entities, param):
        ''' Module which analyzes the understood text and couples them to the found entities in the correct phase. 
            Returns a topic, boolean if next phase should be initiated and the parameters with which this next phase should be initiated. Please not that this is a *hard-coded* conversation, with very limited freedom.
            @TODO: Fix that sentences are analyzed to take "no carbs in food x" into account
        '''
        # Only use lower case
        text = text.lower()
        if self.phase == 1:
            ''' Phase 1:
                Determine which foods on the plate contain carbohydrates. If the list is completed (i.e. comparison with ground truth equals True) then proceed to next phase
            '''
            # If there is a denial in the sentence, i.e. "no carbs in chicken", then the answer might be correct and this should be accounted for, see self.yesno
            if any(x in text for x in self.denial_list):
                self.denial = True 
            else:
                self.denial = False
            for key, value in entities.iteritems():
                # Compare with ground truth:
                flat = self.flatten(self.GroundTruth.dinner_plate)
                if key in flat:
                    if flat[flat.index(key)+1]:
                        self.subjective_truth += ((key, True),)
                        # print(len(self.subjective_truth), len(self.GroundTruth.dinner_plate))
                        if set(self.subjective_truth) == set(self.GroundTruth.dinner_plate):
                            return self.yesno[int(True == (not self.denial))], True
                        else:
                            return self.yesno[int(True == (not self.denial))], False
                    else:
                        self.subjective_truth += ((key, False),)
                        return self.yesno[int(False == (not self.denial))], False 
            else:
                return "unclear", False
            
        if self.phase == 3:
            ''' Phase 3
                Determine how much carbs there are in total
            '''
            # Get total amount of carbs
            total_carbs = self.GroundTruth.ComputeCarbs()
            for key, value in entities.iteritems():
                if "cardinal" in value or "NUM" in value:
                    if self.GroundTruth.Compare(key, total_carbs):
                        return "done", True 
                    else:
                        return "disagree", False
            return "unclear", False

        if self.phase == 4:
            ''' Phase 4
                Determine how much insulin the patient needs to inject, based on the amounts
            '''
            # Compute amount of insulin needed to inject
            total_insulin = self.GroundTruth.ComputeUnits()
            for key, value in entities.iteritems():
                if "cardinal" in value or "NUM" in value:
                    if self.GroundTruth.Compare(key, total_insulin):
                        return "done", True 
                    else:
                        return "disagree", False 
            return "unclear", False

        if self.phase == 5:
            return "finish", True

    def AnalyzeSubDialogue(self, text, entities, param, depth):
        ''' Analyzer specifically designed for a subdialogue, since it required more parameters. Instead of putting these parameters as empty in all other phases, a new method is made which handles this special case.
        '''
        # print(entities)
        if self.phase == 2:
            ''' Phase 2:
                Determine how much the patient is going to eat of all listed carb holding food items.
                For each item, a comparison with the ground truth has to be made. If all items are correctly labelled with the right nutritional value, the next phase can be initiated.
            '''
            # Gather data for this single food item
            if depth == 0:
                self.true_nutrition = self.GroundTruth.GetSingleGroundTruth(param)
                self.subjective_nutrition = [param,None,None]
            
            # Return correct flow for the subdialogue
            # - since the sentence "x carbs per 100 gram" should not take the "100" as the stated cardinal, I make an exception here. The only problem if is the weight IS 100 grams, however we shall not take this into account here.
            for key, value in entities.iteritems():
                # Exclude 100
                if "cardinal" in value or "NUM" in value:
                    try: 
                        b = float(key) == float(100)
                    except:
                        continue
                # Check if value is correct
                # print(key, value, "NUM" in value, "cardinal" in value or "NUM" in value)
                if ("cardinal" in value or "NUM" in value) and not b:
                    # print("jaja", key, value, "NUM" in value)
                    self.subjective_nutrition[depth+1] = float(key)
                    equal = self.GroundTruth.Compare(self.subjective_nutrition[depth+1], self.true_nutrition[depth+1])
                    if equal:
                        if self.GroundTruth.CompareLists(self.subjective_nutrition, self.true_nutrition):
                        # if list(self.true_nutrition) == self.subjective_nutrition:
                            # Correct answer, 
                            # add the true nutrition of this value to a global tuple in the database
                            self.GroundTruth.subjective_nutrition += (tuple(self.subjective_nutrition),)
                            return "affirm", True, True
                        else:
                            return "affirm", False, True
                    else:
                        return "disagree", False, False 
            return "unclear", False, False  

    def Finished(self, b):
        if b:
            print("Dialogue finished")
            return(b)

    def GetPOSTags(self, sentence, language):
        ''' Get the POS tags of the given sentence '''
        self.pos_tags = self.Wordnet.ParseSentence(sentence)
        return self.pos_tags

    def GetEntities(self, sentence):
        ''' Get the entities of the incoming sentence, use the entities to select required response
        '''
        self.Wordnet = wordnet.WordNet()
        self.entities = self.Wordnet.main(sentence)
        return self.entities
