'''
    Gets the input stream from the agent's microphone stream and puts it into an API which turns it into text. Text can then be further processed by any dialogue system.
    ****
    Please note that the current Google Speech Cloud API key is my own. After I passed a certain amount of time, my own credit card will be credited with payments which I would prefer not to happen. Please use with caution or not specify your own API Credentials for any desired service.
    - Johannes
    ****
'''
# Import necessary libraries
import speech_recognition as sr 
import connect
import functools, time, io, os
from naoqi import ALProxy
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

class SpeechToText(object):
    ''' Class which holds the modules which should output a text string from any audio input.
    '''
    def __init__(self, session, audio_from_data, recording, text_input, ip_address, work_dir, audio_file, nao_audio_file, text_input_file, language):
        # Set parameters which class needs to know
        # @TODO: Fix some working directory issues, if they arise
        self.ip_address = ip_address
        self.work_dir = work_dir
        self.audio_file = self.work_dir
        self.nao_audio_file = nao_audio_file
        self.text_input_file = text_input_file
        self.language = language
        # Add counter which does the following:
        # - go through all audio samples if pre-recorded
        # - go through all lines of text if input is a text file
        self.count = 0

        # Get services only if audio_from_data is false
        if not audio_from_data and not text_input:
            self.audio_device = session.service("ALAudioRecorder")
            # Get ALMemory service
            # self.memory_service = connection.GetALMemorySession(session)
        if text_input:
            self.sentences = self.GetTextFromFile(self.text_input_file)

    def SwitchLanguage(self, language):
        self.language = language

    def GetAudioStream(self, filename):
        ''' Module which records sound from the microphones and puts them into a file to be used by a Speech Recogntion API
        '''
        # Ideally, the recording starts when sound is detected and stops when the sound fades again. 
        # @TODO Make it that this happens
        # print("Saving in: " + filename)
        channels = (0,0,1,0)    #
        # Stop recording just in case it was recording already if program was/is broken off
        self.audio_device.stopMicrophonesRecording()
        self.audio_device.startMicrophonesRecording(filename, "wav", 48000, channels)
        time.sleep(3)
        self.audio_device.stopMicrophonesRecording()
        audio = self.GetAudioFile(filename)
        return audio

    def GetAudioFile(self, filename):
        ''' Get the audio file from the NAO robot and copy it to the local machine through scp using sshpass for credentials
        '''
        self.count += 1
        # f = self.audio_file+"sample"+str(self.count)+".wav"
        f = self.audio_file+"audio.wav"
        os.system("sshpass -p qbmt8400 scp nao@"+self.ip_address+":"+filename+" "+f)
        return f

    def GetAudioFromFile(self, audio_dir):
        ''' Get audio from files already on local machine
        '''
        self.count += 1
        filename = self.audio_file+"sample"+str(self.count)+".wav"
        return filename
    
    def GetTextFromFile(self, text_input_file):
        ''' Get the text from the specified file. Each line is a different input sentence '''
        with open(text_input_file) as f:
            content = f.readlines()
        content = [x.strip() for x in content]
        return content
    
    def GetText(self, APIName, audio_from_data, text_input):
        ''' Module that gets a string from an audio file using the chosen API from the Python Speech Recognition library. If test==True, audio files are gathered from a directory.            
        '''
        self.count += 1     # Counter which loops through all lines of text file
        # Get text input
        if text_input:
            # Swap here between text input from the terminal vs. text input from a file
            # @TODO: Make this done in the params.yaml file
            # text = self.sentences[self.count-1]
            text = raw_input(">> ")
            return text
        # Get audio
        if audio_from_data:
            self.audio = self.GetAudioFromFile(self.audio_file)
        else:
            self.audio = self.GetAudioStream(self.nao_audio_file)
        APIlist = ["FreeGoogle", "Google", "IBM"]
        self.recognizer = sr.Recognizer()
        with sr.AudioFile(self.audio) as source:
            audio = self.recognizer.record(source)
        # Use specified API
        if APIName == "Google":
            return self.GoogleSpeechToText(self.audio)
        if APIName == "IBM":
            return self.IBMSpeechToText(audio)
        if APIName == "FreeGoogle":
            return self.FreeGoogleSpeechToText(audio)
        else:
            print("API not recognized, please specify any API from the following: \n")
            print(APIlist)
            return 0            
    
    def FreeGoogleSpeechToText(self, audio):
        ''' Use the free Google API to give a string for given audio file. Uses SpeechRecognition Python library
        '''
        try:
            text = self.recognizer.recognize_google(audio)
            return text
        except sr.UnknownValueError:
            print("Google Speech recognition could not understand audio")
            return 0
        except sr.RequestError as e:
            print("Could not requrest results from Google Speech Recognition service; {0}".format(e))
            return 0

    def GoogleSpeechToText(self, audio):
        ''' Use the Google Cloud Speech API to give a string for given audio file. Uses SpeechRecognition Python library with correct credentials. Credentials are exported before running using the `export GOOGLE_APPLICATION_CREDENTIALS="~/location/of/api_key.json"` command
        '''
        if self.language == "english":
            language_code = 'en-US'
        elif self.language == "dutch":
            language_code = 'nl-NL'
        elif self.language == "french":
            language_code = 'fr-FR'
        google_client = speech.SpeechClient()
        with io.open(audio, 'rb') as audio_file:
            content = audio_file.read()
            audio = types.RecognitionAudio(content=content)
        config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz = 48000,
            language_code=language_code
        )
        responses = google_client.recognize(config, audio)
        print(responses)
        # Gather all alternatives and their confidence levels
        # if there are multiple utterances with high confidence level, concatinate them into a single string
        # if there are no utterences with high confidence level, return None
        reply = ""
        for result in responses.results:
            for alternative in result.alternatives:
                print("Alternative: ", alternative.transcript, "\n Confidence: ", alternative.confidence)
                if alternative.confidence > 0.7:
                    reply += alternative.transcript
        if reply == "":
            return None
        else:
            return reply
            
    def IBMSpeechToText(self, audio):
        ''' Use the IBM API to give a string for given audio file. Uses SpeechRecognition Python library. Credentials are hard-coded here
            @TODO: Place credentials in a file similar to Google Cloud Speech API
        '''
        IBM_USERNAME = "3db578b3-6781-4c3c-851d-3897ca75b700" 
        IBM_PASSWORD = "4vxWB3MdzHpJ"
        try:
            text = self.recognizer.recognize_ibm(audio, username=IBM_USERNAME, password=IBM_PASSWORD)
            return text
        except sr.UnknownValueError:
            print("IBM Speech to Text could not understand audio")
            return 0
        except sr.RequestError as e:
            print("Could not request results from IBM Speech to Text service; {0}".format(e))
            return 0
