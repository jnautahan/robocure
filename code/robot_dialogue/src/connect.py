'''
    Class which both establishes and closes connections between the computer and the NAOqi agent. 
    Keeps track of all communication between the two and makes sure that the connection is taken care of so the other modules can be focussed purely on the evaluation and analysis side of things.
'''
# Import necessary libraries
import argparse, qi, math
from naoqi import ALProxy

class Connect(object):
    ''' Connection class setting and closing down all connnections with the NAOqi agent.
    '''
    def __init__(self, ip_address, port_nr):
        ''' Create application which can be run, such that a session can be initialized. A session connects a PC with the robot (Pepper, Zora) and allows interaction between them. We set our arguments using argparse, where the IP address is needed to connect to the robot.
        Application is run when constructor is called
        '''
        parser = argparse.ArgumentParser()
        parser.add_argument("--ip", type=str, default=ip_address)
        parser.add_argument("--port", type=int, default=port_nr, help="NAOqi port nr.")
        args = parser.parse_args()
        try:
            # Initialize qi framework
            connection_url = "tcp://" + args.ip + ":" + str(args.port)
            self.app = qi.Application(["SpeechToText", "--qi-url=" + connection_url])
        except RuntimeError:
            print("Cannot connec to NAOqi at IP " + args.ip + "on port " + str(args.port) +".\n"
            "Please check your script arguments. Run with -h option for help.")
            sys.exit(1)

        # Start the app and initialize session
        self.StartApp(self.app)
        self.session = self.InitializeSession(self.app)

    def InitializeSession(self, app):
        ''' Get the session of the corresponding app '''
        return app.session

    def StartApp(self, app):
        # Start the application
        print("Starting the application")
        app.start()

    def StopApp(self):
        # Stop the application
        print("Stopping the application")
        self.app.stop()
    
    def GetALMemorySession(self, session):
        # Get the ALMemory service
        self.memory_service = session.service("ALMemory")
        return self.memory_service

    def SubscribeAll(self, session_dict):
        # Subscribe to the necessary sessions
        for key, value in session_dict.iteritems():
            print(key, value)
            key.subscribe(value)
            print("Subscribed to %s"%(value))

    def UnsubscribeAll(self, session_dict):
        # Unsubscribe all current sessions
        for key, value in session_dict:
            key.unsubscribe(value)
            print("Unsubscribed to %s"%(value))
    
    def Close(self):
        # Close the running program
        self.StopApp()