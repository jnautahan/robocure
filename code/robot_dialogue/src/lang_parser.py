'''
    Main parser for sentences
    For any sentence, parse it such that all `important` tokens are extracted. 
    
    If the language is English, we can use WordNet such that for each token, we are able to look into the WordNet database for `connected` words and try to extract an entity from this extensive list.
    If the language is not English, switch to a SpaCy POS tagger and simply return the corresponding POS tag for each token. Important tags (such as nouns) are listed and tokens are only returned if their POS tag is in the list.


    Obvious improvement lie on the NLP side of things which are not easily solvable. Problems are cases such as:
    - Some nouns consist of multiple words
    - Some words have a context dependent meaning/entity (i.e. "morning" is a `measure`, but of time, not of nutriments)
    - Adjectives are always belonging to some noun, whereas the code currently does not take this into account (i.e. "Three slices of bread and a coffee", gives "three" as a `cardinal` however it is clear that it does not emphasize the number of coffees)
    - The input sequence is not correced for typo's
    - ...
    @TODO: {These todo's might be implemented}
    - Use something as a `depth` measure, i.e. what the first important hyponym was and only list this.
    - Add recognization for words consisting of more than 1 token (i.e. coffee bean)
'''
# Import libraries
import nltk 
import spacy 
from nltk.corpus import wordnet

class Parser():
    ''' Parser for any language
        Returns POS tags of any parsed (tokenized) sentence
    '''
    def __init__(self, language):
        disable = ['ner', 'entityrecognizer', 'textcat']
        if language == "english":
            self.nlp = spacy.load("en", disable=disable)
        elif language == "dutch":
            print("Loading the dutch model")
            self.nlp = spacy.load("nl", disable=disable)
        elif language == "french":
            self.nlp = spacy.load("fr", disable=disable)
        else:
            print("Language not recognized. \n Please provide the language as one of the following: english, dutch, french")
            sys.exit(1)
        
        # Initialize the important grammatical POS tags to look for:
        self.list2search = ['NOUN', 'PROPN', 'NUM', 'ADJ']

    def ParseSentence(self, sentence):
        ''' Gather POS tags of sentence '''
        # Convert sentence to unicode, since SpaCy requires that
        if type(sentence) == str:
            sentence = unicode(sentence, 'utf-8')
        sentence = sentence.lower()
        self.pos_tags = {}        
        text = self.nlp(sentence)

        # Merge compounds to correctly lable (some) multi-word nouns
        index = None 
        index_list = [] 
        for token in text:
            # Get index if token gives meaning to other noun, i.e. compound or adjectival modifier
            if token.dep_ == "compound" or token.dep_ == "amod":
                index = token.idx if not index else index   # Get index of the start 
            else:
                if index:
                    # Append positions to index list
                    index_list.append([index, token.idx+len(token)])
                    index = None 
        for locs in index_list:
            text.merge(locs[0], locs[1])
        
        # Get POS tags
        for token in text:
            token_text, token_tag = token.text, token.pos_
            if token_tag in self.list2search:
                self.pos_tags[token_text] = token_tag 
        return self.pos_tags

class WordNet():
    def __init__(self):
        # Initialize the important grammatical structures and entities to look for
        # looking for: [nouns, adjectives, cardinals]
        self.list2search = ['NN', 'NNS', 'NNP', 'NNPS',
            'JJ', 'JJR', 'JJS',
            'CD', 'RB']
        self.entityList = ['size', 'amount', 'quantity', 'unit', 'drink', 'measure', 'cardinal', 'beverage', 'food', 'quality']
        self.posLst = []
        self.nameBreakOff = -5                  # Break off names to get rid of unneccary codes in the name that WordNet gives us

    def get_hypernyms(self, synset):
        ''' Recursive function that determines all hypernyms of a word.
        Function stops when there are no hypernyms left according to WordNet.
        The |= operator indicates a bit-wise union between two sets; similar to bit-wise addition +=.
        The | operator indicates a union between two sets
        '''
        hypernyms = set()
        for hypernym in synset.hypernyms():
            hypernyms |= set(self.get_hypernyms(hypernym))
        return hypernyms | set(synset.hypernyms())

    def get_attributes(self, synset):
        ''' Determine attributes of adjective(s)
        '''
        return set(synset.attributes())

    def choose(self, token):
        ''' Determine which function to use depending on the POS tag of the word
            Output sets which has wordnet.synsets as elements
        '''
        text = token[0]
        pos = token[1]
        out = set()
        if pos == 'CD':
            # Make an exception for cardinals, since I currently cannot find this using WordNet
            out = set(wordnet.synsets("cardinal"))
        elif pos == 'RB':
            # Make exception for adverbs
            out = set(wordnet.synsets('adverb'))
        else:
            for synset in wordnet.synsets(text):
                out |= self.get_hypernyms(synset)
            if out == set():
                for synset in wordnet.synsets(text):
                    out |= self.get_attributes(synset)
        return out

    def search_entities(self, text):
        ''' Make the dictionary containing all the important words
        First tokenize the sentence with NLTK
        Then use nltk.corpus.wordnet to search for entities corresponding to these tokens
        '''
        words = {}
        text = nltk.word_tokenize(text)
        tags = nltk.pos_tag(text)
        # Loop through all tags and search WordNet
        for tag in tags:
            tag_text, tag_pos = tag[0], tag[1]
            if tag_pos in self.list2search:
                self.posLst.append(tag_text)
                words[tag_text] = self.choose(tag)

        # Gather entities and their values in a dictionary
        # !! note that the output is value:key
        #    this is because some entities have multiple values (i.e. "I ate cheese pizza", where both cheese and pizza are `food`)
        #    since dictionaries cannot hold double keys by definition, I reversed the order of the dictionary
        entities = {}
        for key, value in words.items():
            lst = []
            for name in value:
                abbrName = str(name.name()[:self.nameBreakOff])
                if abbrName in self.entityList and abbrName not in lst:
                    lst.append(abbrName)
            entities[str(key)] = lst

        return entities 

    def main(self, sentence):
        entities = self.search_entities(sentence)
        return entities
