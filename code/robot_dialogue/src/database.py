'''
    Searches the specified database for nutritional value of a product;
    database structure is as follow:

    @TODO: Make module that can
    - Search through database given a keyword ("potatoes")
    - Return nutritional value array
    - Parse the input from the IoT to determine what is on the plate and couple to above
'''
# Import necessary libraries
import json, math

class Parameters(object):
    ''' General modules which saves and loads all necessary parameters. Includes methods which allows       comparison between subjective and objective parameters/dictionaries. 
        Initialized with ground truth gotten from the environment (through IoT)
        Ground truth is equal to:
        self.dinner_plate = [[food1, bool1], ..., [foodn, booln]] 
        -> where bool_i is True iff foodn contains carbs
        self.nutrition = [[food1, weight1, carbs1], ..., [foodn, weightn, carbsn]]
        -> where weight_i is the weight in GRAMS, carbs_i the #carbs/100g of food_i

        ** Note: in this proof of concept, this is a JSON file
            -> see LoadGroundTruth for more information if loading from external device
        **
    '''
    def __init__(self, path_to_file, external=False):
        ''' Initialize the ground truth '''
        # Load ground truth from a device:
        if external:
            self.dinner_plate, self.nutrition = self.LoadGroundTruth()
        # Load from local JSON file
        else:
            self.ground_truth = json.load(open(path_to_file))
            self.dinner_plate = ()
            self.nutrition = ()
            for item in self.ground_truth.keys():
                temp = self.ground_truth[item]
                self.dinner_plate += ((item, temp["carbohydrates"]),)
                # self.dinner_plate.append([item, temp["carbohydrates"]])
                if temp["carbohydrates"]:
                    self.nutrition += ((item, float(temp["weight"]), float(temp["carbsper100g"])),)
                    # self.nutrition.append([item, temp["weight"], temp["carbsper100g"]])
        self.subjective_nutrition = ()

    def LoadGroundTruth(self):
        ''' Module which can contain loading the dinner_plate information through another source of         information
        '''
        pass    
        
    def ComputeCarbs(self):
        ''' Compute the total amount of carbs in the food from the subjective nutrition tuple 
            Tuple is built up as follows:
            ((food_item_1, weight_1, carbsper100_1), ...)
            thus we compute
            for i:
                total += weight_i*carbsper100_i/100
        '''
        self.total = 0
        for item in self.subjective_nutrition:
            weight = item[1]
            carbs_density = item[2]/100
            self.total += weight*carbs_density
        return self.total

    def ComputeUnits(self):
        ''' Compute the units of insulin necessary to inject
        '''
        self.unit_per_gram = 1./15
        self.units = self.unit_per_gram*self.total 
        return math.ceil(self.units)

    def ContainsCarbs(self, entities):
        ''' Determine from sentence if it is meant that the entity contains carbs or not
            Future @TODO: Try to fix this using the language parser
        '''
        pass

    def GetSingleGroundTruth(self, param_name):
        ''' Gives the ground truth of a single food item, where it's name is equal to the given param '''
        for item in self.nutrition:
            if param_name in item:
                return item 
        print("Parameter name does not match any in the database, please check spelling.")

    def Compare(self, subjective, truth):
        ''' Compares ground truth with subjective statements
        '''
        subjective = float(subjective)
        truth = float(truth)
        print(subjective, truth)
        # Allow x% off 
        error = 0.05
        domain = [(1-error)*truth, (1+error)*truth]
        # Return true if value is within the margins
        if (1-error)*truth <= subjective <= (1+error)*truth:
            return True
        else:
            return False
    
    def CompareLists(self, subjective, truth):
        ''' Compares ground truth with subjective statements from list '''
        N = len(subjective)
        T = []
        for n in range(0, N):
            try:
                boolean = self.Compare(subjective[n], truth[n])
            except:
                boolean = subjective[n] == truth[n]
            T.append(boolean)
        # Return true if all items are equal
        if all(T):
            return True 
        else:
            return False




    