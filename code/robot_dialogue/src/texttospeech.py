'''
    Outputs the to be spoken sentences such that they are said by the agent
    Output is either in text format through the terminal or sound through the robot
'''

class TextToSpeech(object):
    ''' Class which holds the Text to Speech modules
    '''
    def __init__(self, audio_from_data, session, language):
        # Get services
        self.audio_from_data = audio_from_data
        if not self.audio_from_data:
            self.tts = session.service("ALTextToSpeech")
            # Since ALTextToSpeech needs slightly different string input, alter them here
            if language == "english":
                self.language = "English"
            elif language == "dutch":
                self.language = "Dutch"
            elif language == "french":
                self.language = "French"
            self.tts.setLanguage(self.language)
    
    def SwitchLanguage(self, language):
        self.language = language.title()
        if not self.audio_from_data:
            self.tts.setLanguage(self.language)

    def say(self, sentence, connection_with_robot, var):
        # Say the sentence
        if var:
            sentence = sentence%(var)
        if not connection_with_robot:
            print("> " + sentence)
        else:
            self.tts.say(sentence)
