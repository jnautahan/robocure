#!/bin/bash
#Set home directory:
HOME_DIR=$PWD

#Compile all compiles all necessary LaTeX files for the one who clones the repository
makepdf (){
    pdflatex $1
}
compile (){
    cd $1
    suffix=".tex"; foo=${2%$suffix}
    makepdf $2
    bibtex $foo
    makepdf $2; makepdf $2
    echo "Compiling done"; cd $HOME_DIR 
}

#@TODO: Fix that it uses the up-to-date bibliograph (necessary or not??)
#[NOTE: Below code will also clonde the .git files in that repository, which makes working with git kind of annoying since it generates a git submodule. A better, more simple approach is thus to copy the "updated" bibliography into the repo so that it automatically downloads.]
#Clone the bibliography so that references work:
# BIB_DIR=$HOME_DIR/bibliography
# if ! [ -e "$BIB_DIR/bibliography.bib" ]; then 
#     git clone https://jnautahan@bitbucket.org/jnautahan/bibliography.git 
# fi

#Generate list of items to be compiled
ITEM_ARRAY=( 
    overview_papers.tex 
    ideas.tex 
    presentation.tex
)

#Find items in the list and compile
for item in "${ITEM_ARRAY[@]}"
do 
    temp=$(find $HOME_DIR -name $item)
    # echo ${temp%$item} 
    compile ${temp%$item} $item 
done
