\documentclass{article}
\usepackage[margin=0.5in]{geometry}
\usepackage{algorithm2e}
\usepackage{amsmath}

\newcommand{\tabitem}{~~\llap{\textbullet}~~}

\begin{document} 

    We wish to create an agent which can live alongside humans for a long period of time. Furthermore, it should learn the environment and its behavior autonomously with the goal of being both an addition and not being a hindrance to the humans in co-habitates with. This means that the agent should be able to do the following:
    \begin{itemize}
        \item Navigate a dynamic environment
        \item Retrieve information about the environment for both determining a goal and reaching that goal 
        \item Be able to extract this information through several media, i.e. camera's, lasers, microphones, etc. 
    \end{itemize}
    In order to navigate through the dynamic environment it needs to be able to explore and exploit. By means of physical exploration the agent should be able to localize and build a (dynamic) map of the environment. It needs to be able to reach certain locations inside the environment where the locations are possible decided by external factors, i.e. a command \emph{go to the kitchen}. 

    If one wishes to have long-term co-habitation of an autonomous agent then the most important things are addition and annoyance avoidance. Addition means that the agent should be able to assist its co-habitants in tasks or take over tasks completely. Annoyance avoidance means that it should minimize hindrance while assisting itself or its co-habitants. There is a fine line between these two things and sometimes a certain level of hindrance can be accepted as long as the agents' addition outweighs the annoying moments. For example, a vacuum cleaner robot keeps an environment clear, but it might be in the way while it is vacuuming. Most humans will take this hindrance for granted since it offers them something (a clean house) that outweights the annoyance (getting in the way while cleaning). Furthermore, it is largely subjective what co-habitants find an addition versus an annoyance, therefore the agent should be able to generate some form of \emph{common sense} for what is acceptable within its current environment. This can be achieved by some form of \emph{tabula rasa} learning, where the agent starts with some general framework which is trained and adapted for certain environments. In order to train this common sense the agent first needs to explore generously before it can start to exploit its learned mechanics. 
    
    % \section{Not a control scheme}
    % Formally, control problems are problems that are formulated in such a way that the goal is to reach a certain state while minimizing a cost. This cost is determined by the paths the agents can take, where different paths hold different costs determined by actions. However, in our case the influence of the actions on the current state are \emph{always} unknown. Over time, an agent can develop expectations of the state changes due to performing an action, however it \emph{cannot} predict the outcome perfectly. For example, if an agent asks a question where he expect to get a certain answer, it is always possible that the answer is (slightly) unexpected. However, unexpected answers are not necessarily false such that the agent cannot create an error signal as feedback.

    %%%
    \section{Problem definition}
    Consider an agent navigating in an abstract space, where its goal is to reach a certain state within a goal space.

    %%
    \subsection{Goal states}
    Consider the problem where a select number of goal states $\vec{g}_i$ are known. The goal is to assign values to these goal states such that $\mathbf{g}$ are dictionaries which consist of \emph{key:value} pairs, i.e. $\mathbf{g} = \{g_1:v_1, \ldots, g_n:v_n\}$. The set of goal states is denoted by $\mathcal{G}$ and for our known goal states holds that $\vec{g}_i = (g_1, \ldots, g_m) \in G$ for $i=1, \ldots, n$ with $n$ the number of known goal states. The goal is to gather values $v_j$, where $j= 1,\ldots, m$, for corresponding keys such that the goal is reached if all \emph{key:value} pairs are know. 
    
    %%
    \subsection{Action states}
    Actions can be chosen from the set of legal actions $\mathcal{A}$. Connections between certain actions and states can be formed while learning, however a semi-random action can always be chosen with a certain probablity $p_a$. This is the classical exploration-exploitation dilemma which will be charactarized by $\epsilon$. 
    
    %%
    \subsection{Algorithm sketch}
    \begin{algorithm}
        \KwIn{
            Goal states $\vec{g} = (g_1, \ldots, g_n)$\;
        }
        \KwOut{
            Corresponding dictionary $\mathbf{g} = \{g_1:v_1, \ldots, g_n:v_n\} = \{ \vec{g}:\vec{v} \}$;
        }
        \While{Goal state has undefined key:value pair(s)}{
            % Choose action $a \in \mathbb{A}$
            Choose action $a_t \in \mathcal{A}$\;
            Change state $s_t \rightarrow s_{t+1}$ according to $a_t$\;
            \eIf{$s_{t+1}$ gives a value $v_i$}{
                Add value $v_i$ to goal $g_i$\;
                Choose new action $a_{t+1}$ according to current goal state, previous action $a_t$ and current state $s_{t+1}$\;
            }{
            Choose new action $a_{t+1}$ according to previous action $a_t$ and current state $s_{t+1}$\;
            }
        }
    \end{algorithm}
    
    %%
    \subsection{Example problems}
    The goal of this abstract navigation is that any problem where a dictionary can be generated could be solved. Examples of these type of problems are navigation and question asking. In navigation, the goal is to assign a position to a certain object. In question asking, the goal is to assign values to certain goal states such as the name of a person or street. 

    %
    \subsubsection{Conversations}
    The main problem of conversations is that state changes are incredibly difficult to predict. The result of this is that the influence of the action on the state change is tough to determine. An additional problem is that the number of legal actions is incredibly large. For example, a question which should result in a location of a person can be asked in numerous correct ways; \emph{Where is John?}, \emph{Do you know where John is?}, \emph{Do you know the current location of John?}. Per definition, and due to the fuzzy-ness of language, there is no \emph{correct} question to ask when the goal is to assign a certain value to a goal state. The first large problem therefore needs to address this need of an exceptionally large database if a conversation should be as human-like as possible. However, if one restricts an agent to a certain subject such as \emph{diabetes} or \emph{cooking} the need for a huge database decreases and it might be possible to have a meaningful conversation with only a few actions. 
    \begin{table}[h!]
        \texttt{Conversation 1} \\
        \begin{tabular}{l l}
            \textbf{Goal:}  &  Retrieve nutrional values of food intake during the day \\
            \hline 
            \textbf{Q}:     &  What did you eat today? \\ 
            \textbf{A}:     &  I had breakfast, a nice lunch, a very healty dinner and a dessert afterwards.   \\ 
            \hline 
            \textbf{Q}:     &  That sounds nice! What did you have for breakfast? \\ 
            \textbf{A}:     &  I had a bowl of cereal. \\            
            & \tabitem \emph{Cereal is typically eaten with milk} (implication + knowledge) \\ 
            & \tabitem \emph{How much cereal and milk goes in a bowl?} \\
            \hline 
            \textbf{Q}:     &   And what did you have for lunch? \\ 
            \textbf{A}:     &   I had a sandwich with a cup of tea. \\
            & \tabitem \emph{What kind of sandwich?} \\
            & \tabitem \emph{What size did the sandwich have?} \\
            & \tabitem \emph{Sugar and/or milk in the tea?} \\
            \hline 
            \textbf{Q}:     &   And what did you have for dinner? \\ 
            \textbf{A}:     &   I had pasta bolognese! \\
            & \tabitem \emph{Dinner is usually eaten from a plate} (implication + knowledge) \\
            & \tabitem \emph{What was the size of the plate?} \\ 
            & \tabitem \emph{Did he eat multiple portions?} \\ 
            \hline 
            \textbf{Q}:     &   And as dessert? \\ 
            \textbf{A}:     &   A piece of cake. \\
            & \tabitem \emph{What was the size of the piece?}
        \end{tabular}
        \\
        \texttt{Conversation 2} \\ 
        \begin{tabular}{l l}
            \textbf{Goal:} & Locate a person inside a house \\
            \hline
            \textbf{Q}:     &   Do you know where John is? \\ 
            \textbf{A}:     &   I believe he is upstairs. \\
            \hline 
            \textbf{Q}:     &   Do you know in which room? \\
            \textbf{A}:     &   Last time I saw him he was in his bedroom. \\
            \hline 
        \end{tabular}
    \end{table}

    %
    \subsubsection{Navigation}
    The main difference with conversation is that influence on the state is known in the case of navigation. For example, when an action is to move a certain distance in a certain direction, this is simply done by moving the agent the desired distance. 

    \clearpage
    \section{General framework}
    % Let us now provide a mathematical framework for question asking where the goal is to determine values corresponding to predetermined keys. Goal states are made up of \emph{key:value} pairs where we assume that keys are known. Furthermore, each \emph{key:value} carries a measure of uncertainty where the goal of the question asking is to generate \emph{key:value} pairs with low uncertainty. For example, if a key is \emph{portion size of meal} the question \emph{"How much did you eat?"} might be answered by \emph{"I ate two plates."}. However, the answer does not provide with a definite value and furthermore the amount (two plates) might vary widely. Thus the uncertainty for the key \emph{portion size of meal} is still high and the follow-up question \emph{"How big are your plates?"} might be a reasonable one. 

    Let us assume that we have a Human-Robot Interface (HRI) where the robot seeks values for known keys by performing action. The state of the robot is given by a tuple as follows:
    \begin{align*}
        \mathbf{g} &= (\vec{g}, \vec{v}, \vec{p}) \\
        \text{where \;} &\vec{g} = (g_1, g_2, \ldots, g_n) \text{\; the goal keys} \\
        &\vec{v} = (v_1, v_2, \ldots, v_n) \text{\; the values} \\
        &\vec{p} = (p_1, p_2, \ldots, p_n) \text{\; the measure of certainty, \;} p_i \in [0,1]
    \end{align*}
    where $n$ is the number of goal keys. The robot can perform actions from a set $\mathcal{A}$ which result in a state change. The new state is mapped to the values of the goal keys using assumptions, where the assumptions model the underlying \emph{common sense}. The goal is to get \emph{key:value:certainty} triples where a high certainty should be achieved by means of tuning a probability distribution over all possible values. When tuning is done, the robot can state that these values are correct with high certainty. The algorithm should therefore provide means to tune the probability distribution using actions and the measured state change. 

    However, initially this algorithm is flawed since it cannot say anything with a certain degree of certainty. Furthermore, 

    \subsection{Fuzzy approach}
    Perhaps a better approach would be the application of fuzzy sets. Here, we can assign a membership function over the values' domain and tune the membership function to mimic sharpening the probability distribution. This means that our state now reads:
    \begin{align*}
        \mathbf{g} &= (\vec{g}, \vec{v}, \vec{p}) \\
        \text{where \;} \vec{g} &= (g_1, g_2, \ldots, g_n) \text{\; the goal keys} \\
        \vec{v} &= (v_1, v_2, \ldots, v_n) \text{\; the values} \\
        \vec{m}(\vec{v}) &= (m_1(v_1), m_2(v_2), \ldots, m_n(v_n)) \text{\; the membership function, \;} m_i(v_i) \in [0,1].
    \end{align*}
    Values are assumed to be over some domain, so that the corresponding membership functions are also over this domain. The goal is to tune membership functions such that the set of values for a certain value starts to resemble a crisp set. Ultimately, the value belongs to a crisp set however this will almost never be achievable in practice. For a fuzzy set to become a crisp set it must hold that 
    \begin{align*}
        m(v) \in \{0,1\} \;\forall v,
    \end{align*}
    i.e. the membership function is either $0$ or $1$ for all values a value can take. Resembling a crisp set means that we have to define a measure of \emph{crispness} of a fuzzy set. This crispness should be some value between $0$ and $1$ where $0$ indicates that the set is fuzzy


    \clearpage 
    \section{The impaired agent}
    Let us consider the following experiment with an impaired agent. The agent wishes to determine a goal value in an $n$-dimensional space. This goal value could be anything from nutritional value of a meal or a location of an object inside an environment. The problem is that the agent cannot measure these values directly or gathering these values is costly. For example, when an agent wishes to determine the location of an object it can search everywhere until it finds it, however this search can take a very long time. Therefore, before the agent can conclude anything, it wishes to minimize the uncertainty it has about the goal. Minimizing the uncertainty is done through actions, where actions can be physical as well as abstract. Let us now define the mathematical framework of the situations and illustrate an example problem.

    Actions can be choses from a set of legal actions $\mathcal{A}$, where each action carries a cost $C(a)$ for $a\in\mathcal{A}$. 

    \subsection{Fuzzy entropy}
    Let us consider that the \emph{true} value resides within a domain $X$. Then our fuzzy set $V$ is characterized by a membership function $m_V(x)$ which associates a real number in the interval $[0,1]$ with each element in $X$. Thus, $V = \{X, m_V(x) | x\in X\}$. We furthermore define  


\end{document}