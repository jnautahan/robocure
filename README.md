RoboCure project
================
Directory/repository containing papers, abstracts, summary's and ideas. The main proposal is in the self-explanatory directory and focusses on helping kids with Diabetes Type I to smoothen the transition between the hospital visit and life at home.
Goals:
- Enable long-term autonomy of (Pepper) robot
- Enable life-long learning methods
- Write down summary's and ideas about achieving these and related goals
- Implement (ROS) code that can be executed


Compilation of LaTeX files
==========================
For full compilation of the LaTeX files, execute ```./compile_all.sh```.

Code
====
tba

!TODO
=====
* Identify hardware/sensor limitations of Pepper
* Decide which techniques are sufficient for:
  * navigation
  * conversation


LOG
===
| Date      |   Notes   | @TODO |
| -----     | --------- | ----- |
| 15-12-17  |           |       |
