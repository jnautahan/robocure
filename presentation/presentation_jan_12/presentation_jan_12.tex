% \documentclass[aspectratio=169]{beamer}
\documentclass{beamer}
\usepackage{texmacros}
\usepackage{mathtools}
\usepackage{amssymb}    
\usepackage{framed}
\usepackage{algorithm2e}
%Select beamer theme
% \ugentbeamertheme
\standardbeamertheme

%Title
\title[Literature presentation]{Literature presentation; \\ long-term autonomy}
\subtitle{Long-term autonomy, life-long learning and the fuzziness of the world}
\author{Johannes Nauta}
\date{\today}

\begin{document}

%Titlepage
\frame{\titlepage}
%Provide an overview of the topics to be discussed
\begin{frame}
    \textbf{Goal:} Achieve long-term autonomy for agents which co-habitates with humans \\
    \vspace{0.5cm} 
    What should an agent do?
    \begin{itemize}
        \item Navigate a (dynamic) environment
        \item Gather information by using sensors (camera's, lasers, microphones) for
        \begin{itemize}
            \item Determining a goal 
            \item Reaching a goal 
            \item Adapt
        \end{itemize}  
        \item Interact with the environment
        \begin{itemize}
            \item Conversations 
            \item Education 
            \item Learn 
        \end{itemize}             
    \end{itemize}
\end{frame}

\begin{frame}{Realizing long-term co-habitation}
    Needs to 
    \begin{itemize}
        \item have positive addition
        \item not be annoying 
    \end{itemize}
    \vspace{0.5cm}
    \textbf{Note 1:} Both \emph{subjective} for environment \\
    \textbf{Note 2:} As long as the total addition to an environment is positive, some negatives might be forgiven, \\
    e.g. vacuum robot
\end{frame}

\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=\linewidth]{images/guideline}
        \caption{Taken from \cite{art:leite2013social}}
    \end{figure}
\end{frame}

\begin{frame}{Temporal reasoning}
    \begin{itemize}
        \item Causality over time 
        \item Context awareness
        \item ...
    \end{itemize}
    \textbf{Necessary} for long-term autonomy \\
    \vspace{0.5cm}
    Between MDPs and semi-MDPs: A framework for temporal abstraction in reinforcement learning \cite{art:sutton1999between}
\end{frame}

\begin{frame}{Intermediate goals}
    \centering
    \includegraphics[height=0.8\textheight]{images/lifelongdiagram}
\end{frame}

\begin{frame}
    \textbf{Life-long learning} \\
    $\times$ \emph{not} in the sense of 
    \begin{align*}
        \begin{rcases}
            \text{more data} \\
            \text{more training}
        \end{rcases}
        \Rightarrow \text{better performance}
    \end{align*}
    $\checkmark$ continuous learning by adapting/increasing \emph{skill set}
    \begin{itemize}
        \item transfer learning 
        \item tabula rasa learning 
        \item ...
    \end{itemize}
\end{frame}

%Tabula rasa learning
\begin{frame}{Continuous learning methods, recent work}
    If a general framework exists $\rightarrow$ clean slate (Tabula rasa) learning \\
    If it does not $\rightarrow$ build general framework with transfer learning \\
    \vspace{0.5cm}
    Examples:
    \begin{itemize}
        \item \emph{Tabula rasa}:
        \begin{itemize}
            \item  Mastering Chess and Shogi by Self-Play with a General Reinforcement Learning Algorithm \cite{art:silver2017mastering}
        \end{itemize}
        \item \emph{Transfer learning}:
        \begin{itemize}
            \item Towards a life-long learning soccer agent \cite{in:kleiner2002towards}
            \item Robotic playing for hierarchical complex skill learning \cite{in:hangl2016robotic}
            \item Progressive neural networks \cite{art:rusu2016progressive}
        \end{itemize}
    \end{itemize}
\end{frame}

% \begin{frame}{Current situation}
%     \begin{tabular}{c c}
%         \textbf{Much research}  &   Specific RL tasks \\
%                                 &   Navigation in static environment \\
%                                 &   Classification \\
%         \textbf{Little research}&   
% \end{frame}
\begin{frame}{Current situation}
    Weaknesses
    \begin{itemize}
        \item Ever increasing computational costs 
        \item Very specific problem solving
        \item Disregarding important aspects
    \end{itemize}
    \vspace{0.5cm}
    $\rightarrow$ need agent which (in real-time) can \\
    \begin{itemize}
        \item generate `common sense' \cite{art:minsky2004st} 
        \item `think' like a human \cite{art:lake2017building}
    \end{itemize}
\end{frame}

%Focus: conversations
\begin{frame}{Focus: simple question asking \\
    \emph{[In view of RoboCure project]} }
    Wish to address both in HRI or solo-agent tasks \\
    \vspace{0.5cm}
    \texttt{Example} \\
    \textbf{Goal:} Gather information through asking questions \\
    e.g. \emph{"What did you eat today?"}, \emph{"Where is John?"} \\
    \vspace{0.5cm}
    Wish to extract values from answers \\
    e.g. \emph{"A sandwich"}, \emph{"In the kitchen"} \\
    \vspace{0.5cm}
    However, `human tasks' are (almost) \underbar{always} fuzzy 
\end{frame}

%Fuzzy sets
\begin{frame}
    \textbf{Regular set} \\
    \includegraphics[width=0.85\linewidth]{images/crispexample.png}
\end{frame}
\begin{frame}
    \textbf{Fuzzy set}
    \includegraphics[width=0.9\linewidth]{images/fuzzyexample.png}
\end{frame}

\begin{frame}{Mathematical description of fuzzy sets}
    Formal description by \cite{inc:zadeh1996fuzzy} (actually 1965, this is a reprint) \\
    \vspace{0.5cm}
    Let $X$ be a set with elements denoted by $x$. \\
    A fuzzy set $A$ in $X$ is characterized by a membership function $m_A(x)$ which associates a real number in the interval $[0,1]$ with each element of $X$. The value of $m_A(x)$ represents the degree/grade of membership of $x$ in $A$. Thus, the nearer the value of $m_A(x)$ to unity, the higher the degree of membership of $x$ in $A$. \\
    \vspace{0.5cm}
    \textbf{Note:} A \emph{crisp} set is a set where $m_A(x) = 0$ or $1$.
\end{frame}

\begin{frame}
    Agent has to accept fuzziness up to a certain level \\
    i.e. \emph{"In the middle"}, \emph{"In the cupboard"} is enough \\
    \vspace{0.5cm}
    For tasks $\rightarrow$ assign `likely' values to keys \\
    e.g. $cupboard_{pos} \approx \vec{x}$ \\
    \vspace{0.5cm}
    Need for general framework, including database
\end{frame}

\begin{frame}{General framework}
    \begin{figure}
        \includegraphics[width=\linewidth]{images/common_sense_generation}
        \caption{Dotted lines display a non-crucial set or influence}
    \end{figure}
\end{frame}

%Proposed ideas
\begin{frame}{Pitches}
    \begin{table}
        \begin{tabular}{|l|l|}
            \hline
            Nutrient agent  &   Gather information about nutrients in food  \\
            (moderate/hard) &   \emph{fuzziness}: `bowl', `pasta', ... \\ 
                            &   \emph{requirements}: NLP, Nutrient DB, Recipe DB, ... \\
            \hline
            Exercise agent  &   Gather information about energy consumption \\
            (hard)          &   \emph{fuzziness}: `intensive', `long', ... \\ 
                            &   \emph{requirements}: NLP, Exercise DB, Weight, ... \\ 
            \hline 
            Impaired agent  &   Gather location of trees \\
            (easy/moderate) &   \emph{fuzziness}: `left', `tall', ... \\
                            &   \emph{requirements}: Tree DB [Can make own!] \\
            \hline
        \end{tabular}
    \end{table}
\end{frame}

%Impaired robot
\begin{frame}{Impaired agent}
    Consider that a robot wishes to determine the position of a tree \\ 
    \includegraphics[width=\linewidth]{images/impaired_agent}
\end{frame}
\begin{frame}
    Trees have different
    \begin{itemize}
        \item positioning
        \item height 
    \end{itemize}
    \begin{framed}
        Example of underlying common sense:
        \begin{itemize}
            \item Taller trees tend to the right 
        \end{itemize}
    \end{framed}
    Can we model the generation of this common sense rule?
\end{frame}
\begin{frame}
    Suppose our robot can ask:
    \begin{enumerate}
        \item What is the positioning of the tree?
        \item What is the height of the tree?
    \end{enumerate}
    Where answers consist of:
    \begin{enumerate}
        \item left, middle, right
        \item small, kind of small, kind of tall, tall 
    \end{enumerate}
    \vspace{0.5cm}
    $\rightarrow$
    Answer $2$ carries `more accurate' information, thus desired
\end{frame}
\begin{frame}
    Agent should be `certain' before looking
    \begin{itemize}
        \item High cost associated with looking 
        \item Additional cost per question asked \\
                $\Rightarrow$ ask efficient question(s), then look 
    \end{itemize} 
    \vspace{0.5cm}
    Need some \emph{measure} of fuzziness
\end{frame}
\begin{frame}{Fuzzy control system}
    \includegraphics[width=\linewidth]{images/fuzzy_control} \\
    \vspace{0.5cm}
    In our case 
    \begin{itemize}
        \item Human is fuzzifier
        \item What is defuzzifier?
    \end{itemize}
\end{frame}
\begin{frame}{Idea}
    Consider membership functions of the form 
    \begin{align*}
        m(s) = \max \left\{ m_{min}, \exp \left( \frac{-(x-\mu)^2}{2\sigma^2} \right) \right\},
    \end{align*}
    where $m_{min}$ defines the `fuzziness' of the world \\
    Update step:
    \begin{align*}
        \mu_{n+1} &= \frac{1}{n} \sum_{n} x^{true}_n \\
        \sigma^2_{n+1} &= \frac{1}{n} \sum_{n} \left( x^{true}_n - \mu_{n} \right)
    \end{align*}
    where $n$ is the number of measurements
\end{frame}
\begin{frame}{Algorithm sketch}
    \begin{algorithm}[H]
        \For{$n=1,2,...$}{
        ask question with minimal expected cost \\
        Cost.update() \\
        \While{fuzziness is high}{
            Ask next question \\
            Cost.update() 
        }
        look \\
        highCost.update() \\
        get true coordinates \\
        update $\mu$ and $\sigma$ \\
        update membership function(s)
        }
    \end{algorithm}
\end{frame}
\begin{frame}{Estimated/desired end-result}
    \includegraphics[width=\linewidth]{images/tuned_membership}
\end{frame}
\begin{frame}{Open questions}
    \begin{itemize}
        \item Initialization
        \item Life-long learning $\rightarrow$ add time dependency
        \item Common sense?
        \item ...
    \end{itemize}
\end{frame}
% %
% \begin{frame}
%     Boxes have different
%     \begin{itemize}
%         \item positioning
%         \item colours
%         \item sizes
%     \end{itemize}
%     \begin{framed}
%     Underlying common sense (deterministically):
%     \begin{itemize}
%         \item Red boxes tend to be on the left
%         \item Blue boxes tend to be in the middle
%         \item Green boxes tend to be on the right 
%         \item Size of the box is proportional to position
%     \end{itemize}
%     \end{framed}
%     Can we model the generation of these common sense rules?
% \end{frame}


\bibliographystyle{apalike}
\bibliography{\standardbib}

\end{document}